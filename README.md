## Connecting ##
### With a bot token ###
```java
Token token = new Token("my_bot_token");
DiscordClient client = new DiscordClient(token);
client.connect();
```

### With an email/password ###
```java
Token password = new Token("my_password");
Token token = DiscordClient.login("my_email@example.com", password);
DiscordClient client = new DiscordClient(token);
client.connect();
```