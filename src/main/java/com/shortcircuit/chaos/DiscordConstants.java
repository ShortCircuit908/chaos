package com.shortcircuit.chaos;

/**
 * @author ShortCircuit908
 *         Created on 5/24/2016.
 */
public interface DiscordConstants {
	String VERSION = "1.0.5";
	String USER_AGENT = "Chaos (https://bitbucket.org/ShortCircuit908/chaos/, " + VERSION + ")";
}
