package com.shortcircuit.chaos;

import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

/**
 * @author ShortCircuit908
 *         Created on 6/7/2016.
 */
public class Utils {
	public static String imageToDataUrl(RenderedImage image, String image_format) throws IOException {
		String url = "data:image/" + image_format + ";base64,";
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		Base64OutputStream out = new Base64OutputStream(bout, true, 0, null);
		ImageIO.write(image, image_format, out);
		url += new String(bout.toByteArray());
		return url;
	}

	public static BufferedImage dataUrlToImage(String url) throws IOException {
		String encoding = "base64,";
		url = url.substring(url.indexOf(encoding) + encoding.length());
		ByteArrayInputStream bin = new ByteArrayInputStream(url.getBytes());
		return ImageIO.read(new Base64InputStream(bin, false, 0, null));
	}

	public static Logger newLogger(String name) {
		Logger logger = Logger.getLogger(name);
		logger.setUseParentHandlers(false);
		ConsoleHandler handler = new ConsoleHandler();
		handler.setFormatter(new SimpleLogFormat());
		logger.addHandler(handler);
		return logger;
	}
}
