package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.user.User;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GuildMemberAddEvent extends GuildMemberEvent<GuildMemberAddEvent.EventObject> {
	protected GuildMemberAddEvent() {
		this.event_type = EventType.GUILD_MEMBER_ADD;
	}

	public static class EventObject extends User {
		protected Snowflake guild_id;

		protected EventObject() {

		}

		public Snowflake getGuildId() {
			return guild_id;
		}
	}
}
