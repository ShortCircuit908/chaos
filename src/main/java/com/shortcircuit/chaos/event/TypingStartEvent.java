package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class TypingStartEvent extends ChannelEvent<TypingStartEvent.EventObject> {
	protected TypingStartEvent() {
		this.event_type = EventType.TYPING_START;
	}

	public class EventObject extends GatewayDispatch.EventObject {
		protected Snowflake channel_id;
		protected Snowflake user_id;
		protected long timestamp;

		protected EventObject() {

		}

		public Snowflake getChannelId() {
			return channel_id;
		}

		public Snowflake getUserId() {
			return user_id;
		}

		public long getTimestamp() {
			return timestamp;
		}
	}
}
