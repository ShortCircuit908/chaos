package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class MessageDeleteEvent extends MessageEvent<MessageDeleteEvent.EventObject> {
	protected MessageDeleteEvent() {
		this.event_type = EventType.MESSAGE_DELETE;
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected Snowflake id;
		protected Snowflake channel_id;

		protected EventObject() {

		}

		public Snowflake getMessageId() {
			return id;
		}

		public Snowflake getChannelId() {
			return channel_id;
		}
	}
}
