package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.voice.VoiceState;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class VoiceStateUpdateEvent extends UserEvent<VoiceState> {
	protected VoiceStateUpdateEvent() {
		this.event_type = EventType.VOICE_STATE_UPDATE;
	}
}
