package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.guild.GuildMember;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GuildMembersChunkEvent extends GuildMemberEvent<GuildMembersChunkEvent.EventObject> {
	protected GuildMembersChunkEvent() {
		this.event_type = EventType.GUILD_MEMBERS_CHUNK;
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected Snowflake guild_id;
		protected GuildMember[] members;

		protected EventObject() {

		}

		public Snowflake getGuildId() {
			return guild_id;
		}

		public GuildMember[] getMembers() {
			return members;
		}
	}
}
