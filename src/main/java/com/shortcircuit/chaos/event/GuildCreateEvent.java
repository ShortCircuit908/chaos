package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.guild.Guild;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GuildCreateEvent extends GuildEvent<Guild> {
	protected GuildCreateEvent() {
		this.event_type = EventType.GUILD_CREATE;
	}
}
