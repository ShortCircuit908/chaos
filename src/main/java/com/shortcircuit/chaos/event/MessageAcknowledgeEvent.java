package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/31/2016.
 */
public class MessageAcknowledgeEvent extends MessageEvent<MessageAcknowledgeEvent.EventObject> {
	protected MessageAcknowledgeEvent() {
		this.event_type = EventType.MESSAGE_ACK;
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected Snowflake message_id;
		protected Snowflake channel_id;

		protected EventObject() {

		}

		public Snowflake getMessageId() {
			return message_id;
		}

		public Snowflake getChannelId() {
			return channel_id;
		}
	}
}
