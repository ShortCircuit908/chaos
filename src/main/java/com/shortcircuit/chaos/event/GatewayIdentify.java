package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Token;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GatewayIdentify extends GatewayDispatch<GatewayIdentify.EventObject> {
	public GatewayIdentify() {
		this.opcode = Opcodes.IDENTIFY.opcode;
	}

	public GatewayIdentify(EventObject data) {
		this();
		this.data = data;
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected Token token;
		protected ClientProperties properties;
		protected boolean compress;
		protected Integer large_threshold;
		protected int[] shard;

		public EventObject(Token token, ClientProperties properties) {
			this.token = token;
			this.properties = properties;
		}

		public EventObject(Token token, ClientProperties properties, boolean compress) {
			this(token, properties);
			this.compress = compress;
		}

		public EventObject(Token token, ClientProperties properties, boolean compress, Integer large_threshold, int[] shard) {
			this(token, properties);
			this.compress = compress;
			this.large_threshold = large_threshold;
			this.shard = shard;
		}

		public Token getToken() {
			return token;
		}

		public ClientProperties getProperties() {
			return properties;
		}

		public boolean useCompression() {
			return compress;
		}

		public int getLargeThreshold() {
			return large_threshold;
		}

		public int[] getShard() {
			return shard;
		}
	}

	public static class ClientProperties {
		public String $os;
		public String $browser;
		public String $device;
		public String $referrer;
		public String $referring_domain;

		public ClientProperties(String os, String browser, String device, String referrer, String referring_domain) {
			this.$os = os;
			this.$browser = browser;
			this.$device = device;
			this.$referrer = referrer;
			this.$referring_domain = referring_domain;
		}

		public String getOs() {
			return $os;
		}

		public String getBrowser() {
			return $browser;
		}

		public String getDevice() {
			return $device;
		}

		public String getReferrer() {
			return $referrer;
		}

		public String getReferringDomain() {
			return $referring_domain;
		}
	}
}
