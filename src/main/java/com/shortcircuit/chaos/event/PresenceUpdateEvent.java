package com.shortcircuit.chaos.event;

import com.google.gson.annotations.SerializedName;
import com.shortcircuit.chaos.obj.Game;
import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.user.User;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class PresenceUpdateEvent extends UserEvent<PresenceUpdateEvent.EventObject> {
	protected PresenceUpdateEvent() {
		this.event_type = EventType.PRESENCE_UPDATE;
	}

	public enum Status {
		@SerializedName("idle")
		IDLE,
		@SerializedName("online")
		ONLINE,
		@SerializedName("offline")
		OFFLINE
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected User user;
		protected Snowflake[] roles;
		protected Game game;
		protected Snowflake guild_id;
		protected Status status;

		protected EventObject() {

		}

		public User getUser() {
			return user;
		}

		public Snowflake[] getRoles() {
			return roles;
		}

		public Game getGame() {
			return game;
		}

		public Snowflake getGuildId() {
			return guild_id;
		}

		public Status getStatus() {
			return status;
		}
	}
}
