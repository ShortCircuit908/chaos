package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.channel.Message;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class MessageUpdateEvent extends MessageEvent<Message> {
	protected MessageUpdateEvent() {
		this.event_type = EventType.MESSAGE_UPDATE;
	}
}
