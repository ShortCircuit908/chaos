package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GuildRoleDeleteEvent extends GuildRoleEvent<GuildRoleDeleteEvent.EventObject> {
	protected GuildRoleDeleteEvent() {
		this.event_type = EventType.GUILD_ROLE_DELETE;
	}

	public static class EventObject extends GuildRoleEvent.EventObject {
		protected Snowflake role_id;

		protected EventObject() {

		}

		public Snowflake getRoleId() {
			return role_id;
		}
	}
}
