package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.channel.DMChannel;
import com.shortcircuit.chaos.obj.channel.GuildChannel;
import com.shortcircuit.chaos.obj.channel.TextChannel;
import com.shortcircuit.chaos.obj.channel.VoiceChannel;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class ChannelDeleteEvent extends ChannelEvent<ChannelCreateEvent.EventObject> {
	@Exclude
	private DMChannel dm_channel;
	@Exclude
	private TextChannel text_channel;
	@Exclude
	private VoiceChannel voice_channel;

	protected ChannelDeleteEvent() {
		event_type = EventType.CHANNEL_DELETE;
	}

	public boolean isPrivateChannel() {
		return data.isPrivate();
	}

	public boolean isTextChannel() {
		return !data.is_private && data.type == GuildChannel.Type.TEXT;
	}

	public boolean isVoiceChannel() {
		return !data.is_private && data.type == GuildChannel.Type.VOICE;
	}

	public DMChannel getDMChannel() {
		if (!isPrivateChannel()) {
			throw new IllegalStateException("payload is not a DMChannel");
		}
		return dm_channel == null ? (dm_channel = new DMChannel(data.id, data.recipient, data.last_message_id)) : dm_channel;
	}

	public TextChannel getTextChannel() {
		if (!isTextChannel()) {
			throw new IllegalStateException("payload is not a TextChannel");
		}
		return text_channel == null ? (text_channel = new TextChannel(data.id, data.guild_id, data.name, data.position, data.permission_overwrites, data.topic, data.last_message_id)) : text_channel;
	}

	public VoiceChannel getVoiceChannel() {
		if (!isVoiceChannel()) {
			throw new IllegalStateException("payload is not a VoiceChannel");
		}
		return voice_channel == null ? (voice_channel = new VoiceChannel(data.id, data.guild_id, data.name, data.position, data.permission_overwrites, data.bitrate)) : voice_channel;
	}
}
