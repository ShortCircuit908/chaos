package com.shortcircuit.chaos.event;

/**
 * @author ShortCircuit908
 *         Created on 5/31/2016.
 */
public class ClientDisconnectEvent extends GatewayDispatch<Boolean> {

	public ClientDisconnectEvent(boolean complete) {
		this.opcode = -1;
		this.data = complete;
	}

	public boolean isComplete() {
		return data;
	}
}
