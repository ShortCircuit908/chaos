package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.channel.GuildChannel;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class ChannelUpdateEvent extends ChannelEvent<GuildChannel> {
	protected ChannelUpdateEvent() {
		event_type = EventType.CHANNEL_UPDATE;
	}
}
