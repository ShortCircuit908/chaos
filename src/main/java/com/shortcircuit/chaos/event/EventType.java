package com.shortcircuit.chaos.event;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public enum EventType {
	READY(ReadyEvent.class),
	CHANNEL_CREATE(ChannelCreateEvent.class),
	CHANNEL_UPDATE(ChannelUpdateEvent.class),
	CHANNEL_DELETE(ChannelDeleteEvent.class),
	GUILD_BAN_ADD(GuildBanAddEvent.class),
	GUILD_BAN_REMOVE(GuildBanRemoveEvent.class),
	GUILD_CREATE(GuildCreateEvent.class),
	GUILD_UPDATE(GuildUpdateEvent.class),
	GUILD_EMOJI_UPDATE(GuildEmojiUpdateEvent.class),
	GUILD_DELETE(GuildDeleteEvent.class),
	GUILD_INTEGRATIONS_UPDATE(GuildIntegrationsUpdateEvent.class),
	GUILD_MEMBER_ADD(GuildMemberAddEvent.class),
	GUILD_MEMBER_REMOVE(GuildMemberRemoveEvent.class),
	GUILD_MEMBER_UPDATE(GuildMemberUpdateEvent.class),
	GUILD_MEMBERS_CHUNK(GuildMembersChunkEvent.class),
	GUILD_ROLE_CREATE(GuildRoleCreateEvent.class),
	GUILD_ROLE_UPDATE(GuildRoleUpdateEvent.class),
	GUILD_ROLE_DELETE(GuildRoleDeleteEvent.class),
	MESSAGE_ACK(MessageAcknowledgeEvent.class),
	MESSAGE_CREATE(MessageCreateEvent.class),
	MESSAGE_UPDATE(MessageUpdateEvent.class),
	MESSAGE_DELETE(MessageDeleteEvent.class),
	MESSAGE_REACTION_ADD(MessageReactionAddEvent.class),
	MESSAGE_REACTION_REMOVE(MessageReactionRemoveEvent.class),
	PRESENCE_UPDATE(PresenceUpdateEvent.class),
	RESUMED(ResumeEvent.class),
	TYPING_START(TypingStartEvent.class),
	USER_SETTINGS_UPDATE(UserSettingsUpdateEvent.class),
	USER_UPDATE(UserUpdateEvent.class),
	VOICE_STATE_UPDATE(VoiceStateUpdateEvent.class),
	VOICE_SERVER_UPDATE(VoiceServerUpdateEvent.class);

	public final Class<? extends GatewayDispatch> event_class;

	EventType(Class<? extends GatewayDispatch> event_class) {
		this.event_class = event_class;
	}
}
