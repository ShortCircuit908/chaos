package com.shortcircuit.chaos.event;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public abstract class GuildEvent<T> extends GatewayDispatch<T> {
	protected GuildEvent() {

	}
}
