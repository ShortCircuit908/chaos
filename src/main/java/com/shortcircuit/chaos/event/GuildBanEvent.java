package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.user.User;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public abstract class GuildBanEvent extends GuildEvent<GuildBanEvent.EventObject> {
	protected GuildBanEvent() {

	}

	public static class EventObject extends User {
		protected Snowflake guild_id;

		protected EventObject() {

		}

		public Snowflake getGuildId() {
			return guild_id;
		}
	}
}
