package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public abstract class GuildRoleEvent<T> extends GuildEvent<T> {
	protected GuildRoleEvent() {

	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected Snowflake guild_id;

		protected EventObject() {

		}

		public Snowflake getGuildId() {
			return guild_id;
		}
	}
}
