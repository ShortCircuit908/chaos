package com.shortcircuit.chaos.event;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GatewayHeartbeatAcknowledge extends GatewayDispatch<Void> {
	public GatewayHeartbeatAcknowledge() {
		this.opcode = Opcodes.HEARTBEAT_ACK.opcode;
	}
}
