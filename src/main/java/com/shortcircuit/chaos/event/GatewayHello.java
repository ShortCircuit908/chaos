package com.shortcircuit.chaos.event;

import com.google.gson.annotations.SerializedName;

/**
 * @author ShortCircuit908
 *         Created on 9/12/2016.
 */
public class GatewayHello extends GatewayDispatch<GatewayHello.EventObject> {

	public GatewayHello() {
		this.opcode = Opcodes.HELLO.opcode;
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected int heartbeat_interval;
		@SerializedName("_trace")
		protected String[] trace;

		public int getHeartbeatInterval() {
			return heartbeat_interval;
		}

		public String[] getTrace() {
			return trace;
		}
	}
}
