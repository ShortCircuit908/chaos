package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.user.User;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GuildMemberRemoveEvent extends GuildMemberEvent<GuildMemberRemoveEvent.EventObject> {
	protected GuildMemberRemoveEvent() {
		this.event_type = EventType.GUILD_MEMBER_REMOVE;
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected Snowflake guild_id;
		protected User user;

		protected EventObject() {

		}

		public Snowflake getGuildId() {
			return guild_id;
		}

		public User getUser() {
			return user;
		}
	}
}
