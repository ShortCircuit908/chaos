package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GatewayVoiceStateUpdate extends GatewayDispatch<GatewayVoiceStateUpdate.EventObject> {
	public GatewayVoiceStateUpdate() {
		this.opcode = Opcodes.VOICE_STATE_UPDATE.opcode;
	}

	public GatewayVoiceStateUpdate(GatewayVoiceStateUpdate.EventObject data) {
		this();
		this.data = data;
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected Snowflake guild_id;
		protected Snowflake channel_id;
		protected boolean self_mute;
		protected boolean self_deaf;

		public EventObject(Snowflake guild_id, Snowflake channel_id, boolean self_mute, boolean self_deaf) {
			this.guild_id = guild_id;
			this.channel_id = channel_id;
			this.self_mute = self_mute;
			this.self_deaf = self_deaf;
		}

		public Snowflake getGuildId() {
			return guild_id;
		}

		public Snowflake getChannelId() {
			return channel_id;
		}

		public boolean isSelfMute() {
			return self_mute;
		}

		public boolean isSelfDeaf() {
			return self_deaf;
		}
	}
}
