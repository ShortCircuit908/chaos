package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GatewayRequestGuildMembers extends GatewayDispatch<GatewayRequestGuildMembers.EventObject> {
	public GatewayRequestGuildMembers() {
		this.opcode = Opcodes.REQUEST_GUILD_MEMBERS.opcode;
	}

	public GatewayRequestGuildMembers(EventObject data) {
		this();
		this.data = data;
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected Snowflake guild_id;
		protected String query;
		protected int limit;

		public EventObject(Snowflake guild_id, String query) {
			this(guild_id, query, 0);
		}

		public EventObject(Snowflake guild_id, String query, int limit) {
			this.guild_id = guild_id;
			this.query = query;
			this.limit = limit;
		}

		public Snowflake getGuildId() {
			return guild_id;
		}

		public String getQuery() {
			return query;
		}

		public int getLimit() {
			return limit;
		}
	}
}
