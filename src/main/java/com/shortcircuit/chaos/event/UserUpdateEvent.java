package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.user.User;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class UserUpdateEvent extends UserEvent<User> {
	protected UserUpdateEvent() {
		this.event_type = EventType.USER_UPDATE;
	}
}
