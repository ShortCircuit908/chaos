package com.shortcircuit.chaos.event;

import com.google.gson.*;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.shortcircuit.chaos.DiscordClient;
import com.shortcircuit.chaos.exception.DiscordException;
import com.shortcircuit.chaos.exception.PacketParseException;
import com.shortcircuit.chaos.exception.UnknownEventException;
import com.shortcircuit.chaos.exception.UnknownOpcodeException;
import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.Token;

import java.io.IOException;
import java.text.DateFormat;
import java.util.UUID;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public abstract class GatewayDispatch<T> {
	public static final Gson GSON = new GsonBuilder()
			.enableComplexMapKeySerialization()
			.setDateFormat(DateFormat.FULL)
			.addSerializationExclusionStrategy(new ExclusionStrategy() {
				@Override
				public boolean shouldSkipField(FieldAttributes f) {
					return f.getAnnotation(Exclude.class) != null
							|| f.getDeclaredClass().isAnnotationPresent(Exclude.class)
							|| f.getDeclaringClass().isAnnotationPresent(Exclude.class);
				}

				@Override
				public boolean shouldSkipClass(Class<?> clazz) {
					return clazz.isAnnotationPresent(Exclude.class);
				}
			})
			.registerTypeAdapter(UUID.class, new TypeAdapter<UUID>() {
				@Override
				public void write(JsonWriter out, UUID value) throws IOException {
					out.value(value == null ? null : value.toString().replace("-", ""));
				}

				@Override
				public UUID read(JsonReader in) throws IOException {
					if (in.peek() == JsonToken.NULL) {
						in.nextNull();
						return null;
					}
					return UUID.fromString(in.nextString().replaceAll("(.{8})-?(.{4})-?(.{4})-?(.{4})-?(.{12})", "$1-$2-$3-$4-$5"));
				}
			})
			.registerTypeAdapter(Token.class, new TypeAdapter<Token>() {
				@Override
				public void write(JsonWriter writer, Token token) throws IOException {
					writer.value(token == null || token.getToken() == null ? null : new String(token.getToken()));
				}

				@Override
				public Token read(JsonReader reader) throws IOException {
					if (reader.peek() == JsonToken.NULL) {
						reader.nextNull();
						return null;
					}
					reader.beginObject();
					Token token = null;
					while (reader.hasNext()) {
						if (reader.nextName().equals("token")) {
							token = new Token(reader.nextString().toCharArray());
							break;
						}
					}
					reader.endObject();
					return token;
				}
			})
			.registerTypeAdapter(Snowflake.class, new TypeAdapter<Snowflake>() {
				@Override
				public void write(JsonWriter writer, Snowflake token) throws IOException {
					writer.value(token == null ? null : token.getUnsignedSnowflake());
				}

				@Override
				public Snowflake read(JsonReader reader) throws IOException {
					if (reader.peek() == JsonToken.NULL) {
						reader.nextNull();
						return null;
					}
					return new Snowflake(reader.nextString());
				}
			})
			//.setDateFormat(DiscordConstants.TIMESTAMP_FORMAT)
			.create();

	@SerializedName("op")
	protected int opcode;
	@SerializedName("d")
	protected T data;
	@SerializedName("t")
	protected EventType event_type;
	@Exclude
	private DiscordClient client;
	@SerializedName("s")
	private Integer sequence_num;

	public GatewayDispatch() {
		this.opcode = Opcodes.DISPATCH.opcode;
	}

	public static GatewayDispatch fromJson(String json) throws DiscordException {
		DummyDispatch dummy = GSON.fromJson(json, DummyDispatch.class);
		try {
			switch (dummy.op) {
				case 0:
					if (dummy.t == null || dummy.t.event_class == null){
						throw new UnknownEventException(json);
					}
					return GSON.fromJson(json, dummy.t.event_class);
				case 1:
					return GSON.fromJson(json, GatewayHeartbeat.class);
				case 2:
					return GSON.fromJson(json, GatewayIdentify.class);
				case 3:
					return GSON.fromJson(json, GatewayStatusUpdate.class);
				case 4:
					return GSON.fromJson(json, GatewayVoiceStateUpdate.class);
				case 5:
					return GSON.fromJson(json, GatewayVoiceServerPing.class);
				case 6:
					return GSON.fromJson(json, GatewayResume.class);
				case 7:
					return GSON.fromJson(json, GatewayReconnect.class);
				case 8:
					return GSON.fromJson(json, GatewayRequestGuildMembers.class);
				case 9:
					return GSON.fromJson(json, GatewayInvalidSession.class);
				case 10:
					return GSON.fromJson(json, GatewayHello.class);
				case 11:
					return GSON.fromJson(json, GatewayHeartbeatAcknowledge.class);
			}
		}
		catch (Exception e) {
			throw new PacketParseException(json, e);
		}
		throw new UnknownOpcodeException(dummy.op, json);
	}

	public DiscordClient getClient() {
		return client;
	}

	protected void setClient(DiscordClient client) {
		this.client = client;
	}

	public int getOpcode() {
		return opcode;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public int getSequence() {
		return sequence_num == null ? -1 : sequence_num;
	}

	public EventType getEventType() {
		return event_type;
	}

	public String toJson() {
		return GSON.toJson(this, this.getClass());
	}

	public static class EventObject {

	}

	static class DummyDispatch {
		protected EventType t;
		protected int op;
	}
}
