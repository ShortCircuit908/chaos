package com.shortcircuit.chaos.event;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GatewayHeartbeat extends GatewayDispatch<Integer> {
	public GatewayHeartbeat() {
		this.opcode = Opcodes.HEARTBEAT.opcode;
	}

	public GatewayHeartbeat(int seq) {
		this();
		this.data = seq;
	}
}
