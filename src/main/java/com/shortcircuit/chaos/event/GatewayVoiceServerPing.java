package com.shortcircuit.chaos.event;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GatewayVoiceServerPing extends GatewayDispatch<Void> {
	public GatewayVoiceServerPing() {
		this.opcode = Opcodes.VOICE_SERVER_PING.opcode;
	}
}
