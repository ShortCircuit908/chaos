package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.permissions.Role;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GuildRoleCreateEvent extends GuildRoleEvent {
	protected GuildRoleCreateEvent() {
		this.event_type = EventType.GUILD_ROLE_CREATE;
	}

	public static class EventObject extends GuildRoleEvent.EventObject {
		protected Role role;

		protected EventObject() {

		}

		public Role getRole() {
			return role;
		}
	}
}
