package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Token;

import java.util.UUID;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GatewayResume extends GatewayDispatch<GatewayResume.EventObject> {
	protected GatewayResume() {
		this.opcode = Opcodes.RESUME.opcode;
	}

	public GatewayResume(EventObject data) {
		this();
		this.data = data;
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected Token token;
		protected UUID session_id;
		protected int seq;

		public EventObject(Token token, UUID session_id, int seq) {
			this.token = token;
			this.session_id = session_id;
			this.seq = seq;
		}

		public Token getToken() {
			return token;
		}

		public UUID getSessionId() {
			return session_id;
		}

		public int getSeq() {
			return seq;
		}
	}
}
