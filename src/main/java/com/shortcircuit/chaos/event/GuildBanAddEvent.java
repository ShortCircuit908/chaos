package com.shortcircuit.chaos.event;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GuildBanAddEvent extends GuildBanEvent {
	protected GuildBanAddEvent() {
		this.event_type = EventType.GUILD_BAN_ADD;
	}
}
