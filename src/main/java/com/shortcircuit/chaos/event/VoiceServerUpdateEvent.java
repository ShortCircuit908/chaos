package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.Token;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class VoiceServerUpdateEvent extends UserEvent<VoiceServerUpdateEvent.EventObject> {
	protected VoiceServerUpdateEvent() {
		this.event_type = EventType.VOICE_SERVER_UPDATE;
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected Token token;
		protected Snowflake guild_id;
		protected String endpoint;

		protected EventObject() {

		}

		public Token getToken() {
			return token;
		}

		public Snowflake getGuildId() {
			return guild_id;
		}

		public String getEndpoint() {
			return endpoint;
		}
	}
}
