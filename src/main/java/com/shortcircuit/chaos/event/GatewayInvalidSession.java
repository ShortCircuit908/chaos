package com.shortcircuit.chaos.event;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GatewayInvalidSession extends GatewayDispatch<Boolean> {
	public GatewayInvalidSession() {
		this.opcode = Opcodes.INVALID_SESSION.opcode;
	}
}
