package com.shortcircuit.chaos.event;

import com.google.gson.annotations.SerializedName;
import com.shortcircuit.chaos.obj.ClientSettings;
import com.shortcircuit.chaos.obj.channel.DMChannel;
import com.shortcircuit.chaos.obj.guild.UnavailableGuild;
import com.shortcircuit.chaos.obj.user.User;

import java.util.UUID;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class ReadyEvent extends GatewayDispatch<ReadyEvent.EventObject> {
	protected ReadyEvent() {
		this.event_type = EventType.READY;
	}

	public static class EventObject extends ResumeEvent.EventObject {
		protected int v;
		protected User user;
		protected DMChannel[] private_channels;
		protected UnavailableGuild[] guilds;
		protected UUID session_id;
		protected int[] shard;
		@SerializedName("user_settings")
		protected ClientSettings client_settings;

		protected EventObject() {

		}

		public int getGatewayProtocolVersion() {
			return v;
		}

		public User getUser() {
			return user;
		}

		public DMChannel[] getPrivateChannels() {
			return private_channels;
		}

		public UnavailableGuild[] getGuilds() {
			return guilds;
		}

		public UUID getSessionId() {
			return session_id;
		}

		public int[] getShard() {
			return shard;
		}
	}
}
