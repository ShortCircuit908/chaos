package com.shortcircuit.chaos.event;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GatewayReconnect extends GatewayDispatch<Void> {
	public GatewayReconnect() {
		this.opcode = Opcodes.RECONNECT.opcode;
	}
}
