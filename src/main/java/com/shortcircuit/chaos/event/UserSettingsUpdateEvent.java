package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.user.UserSettings;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class UserSettingsUpdateEvent extends UserEvent<UserSettings> {
	protected UserSettingsUpdateEvent() {
		this.event_type = EventType.USER_SETTINGS_UPDATE;
	}
}
