package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.DiscordClient;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class EventDispatcher {
	private final LinkedList<Object> listeners = new LinkedList<>();
	private final DiscordClient client;

	public EventDispatcher(DiscordClient client) {
		this.client = client;
	}

	public void registerListener(Object listener) throws IllegalArgumentException {
		if (listeners.contains(listener)) {
			throw new IllegalArgumentException("listener is already registered");
		}
		boolean has_listener_method = false;
		for (Method method : listener.getClass().getDeclaredMethods()) {
			if (!method.isAnnotationPresent(EventHandler.class)
					|| method.getParameterCount() != 1
					|| !GatewayDispatch.class.isAssignableFrom(method.getParameterTypes()[0])) {
				continue;
			}
			has_listener_method = true;
		}
		if (!has_listener_method) {
			throw new IllegalArgumentException("object has no valid listener methods");
		}
		synchronized (listeners) {
			listeners.add(listener);
		}
		client.getLogger().info("Registered listener " + listener.getClass().getName() + "@" + Integer.toHexString(listener.hashCode()));
	}

	public boolean unregisterListener(Object listener) {
		boolean success;
		synchronized (listeners) {
			success = listeners.remove(listener);
		}
		if (success) {
			client.getLogger().info("Unregistered listener " + listener.getClass().getName() + "@" + Integer.toHexString(listener.hashCode()));
		}
		return success;
	}

	public void dispatchEvent(GatewayDispatch event) {
		event.setClient(client);
		synchronized (listeners) {
			for (Object listener : new HashSet<>(listeners)) {
				for (Method method : listener.getClass().getDeclaredMethods()) {
					if (!method.isAnnotationPresent(EventHandler.class)
							|| method.getParameterCount() != 1
							|| !method.getParameterTypes()[0].isAssignableFrom(event.getClass())) {
						continue;
					}
					method.setAccessible(true);
					try {
						method.invoke(listener, event);
					}
					catch (IllegalAccessException | InvocationTargetException e) {
						if (e.getCause() != null) {
							e.getCause().printStackTrace();
						}
						else {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
}
