package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GuildMemberUpdateEvent extends GuildMemberEvent<GuildMemberUpdateEvent.EventObject> {
	protected GuildMemberUpdateEvent() {
		this.event_type = EventType.GUILD_MEMBER_UPDATE;
	}

	public static class EventObject extends GuildMemberRemoveEvent.EventObject {
		protected Snowflake[] roles;
		protected String nick;

		protected EventObject() {

		}

		public Snowflake[] getRoles() {
			return roles;
		}

		public String getNick() {
			return nick;
		}
	}
}
