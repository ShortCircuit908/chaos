package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.channel.*;
import com.shortcircuit.chaos.obj.user.User;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class ChannelCreateEvent extends ChannelEvent<ChannelCreateEvent.EventObject> {
	@Exclude
	private DMChannel dm_channel;
	@Exclude
	private TextChannel text_channel;
	@Exclude
	private VoiceChannel voice_channel;

	protected ChannelCreateEvent() {
		event_type = EventType.CHANNEL_CREATE;
	}

	public boolean isPrivateChannel() {
		return data.is_private;
	}

	public boolean isTextChannel() {
		return !data.is_private && data.type == GuildChannel.Type.TEXT;
	}

	public boolean isVoiceChannel() {
		return !data.is_private && data.type == GuildChannel.Type.VOICE;
	}

	public DMChannel getDMChannel() {
		if (!isPrivateChannel()) {
			throw new IllegalStateException("payload is not a DMChannel");
		}
		return dm_channel == null ? (dm_channel = new DMChannel(data.id, data.recipient, data.last_message_id)) : dm_channel;
	}

	public TextChannel getTextChannel() {
		if (!isTextChannel()) {
			throw new IllegalStateException("payload is not a TextChannel");
		}
		return text_channel == null ? (text_channel = new TextChannel(data.id, data.guild_id, data.name, data.position, data.permission_overwrites, data.topic, data.last_message_id)) : text_channel;
	}

	public VoiceChannel getVoiceChannel() {
		if (!isVoiceChannel()) {
			throw new IllegalStateException("payload is not a VoiceChannel");
		}
		return voice_channel == null ? (voice_channel = new VoiceChannel(data.id, data.guild_id, data.name, data.position, data.permission_overwrites, data.bitrate)) : voice_channel;
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected Snowflake id;
		protected boolean is_private;
		protected User recipient;
		protected String topic;
		protected Snowflake last_message_id;
		protected int bitrate;
		protected Snowflake guild_id;
		protected String name;
		protected GuildChannel.Type type;
		protected int position;
		protected Overwrite[] permission_overwrites;

		protected EventObject() {

		}

		public Snowflake getId() {
			return id;
		}

		public boolean isPrivate() {
			return is_private;
		}

		public User getRecipient() {
			return recipient;
		}

		public String getTopic() {
			return topic;
		}

		public Snowflake getLastMessageId() {
			return last_message_id;
		}

		public int getBitrate() {
			return bitrate;
		}

		public Snowflake getGuildId() {
			return guild_id;
		}

		public String getName() {
			return name;
		}

		public GuildChannel.Type getType() {
			return type;
		}

		public int getPosition() {
			return position;
		}

		public Overwrite[] getPermissionOverwrites() {
			return permission_overwrites;
		}
	}
}
