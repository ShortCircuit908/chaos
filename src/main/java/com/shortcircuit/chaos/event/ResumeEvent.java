package com.shortcircuit.chaos.event;

import com.google.gson.annotations.SerializedName;

/**
 * @author ShortCircuit908
 *         Created on 6/7/2016.
 */
public class ResumeEvent extends GatewayDispatch<ResumeEvent.EventObject> {
	protected ResumeEvent() {
		this.event_type = EventType.RESUMED;
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		@SerializedName("_trace")
		protected String[] trace;

		public String[] getTrace() {
			return trace;
		}
	}
}
