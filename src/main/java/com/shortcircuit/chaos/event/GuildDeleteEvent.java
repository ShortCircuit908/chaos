package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GuildDeleteEvent extends GuildEvent<GuildDeleteEvent.EventObject> {
	protected GuildDeleteEvent() {
		this.event_type = EventType.GUILD_DELETE;
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected Snowflake id;
		protected Boolean unavailable;

		protected EventObject() {

		}

		public Snowflake getGuildId() {
			return id;
		}

		public boolean isUserRemoved() {
			return unavailable == null;
		}

		public boolean isUnavailable() {
			return unavailable;
		}
	}
}
