package com.shortcircuit.chaos.event;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GuildBanRemoveEvent extends GuildBanEvent {
	protected GuildBanRemoveEvent() {
		this.event_type = EventType.GUILD_BAN_REMOVE;
	}
}
