package com.shortcircuit.chaos.event;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public abstract class UserEvent<T> extends GatewayDispatch<T> {
	protected UserEvent() {

	}
}
