package com.shortcircuit.chaos.event;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GuildRoleUpdateEvent extends GuildRoleEvent<GuildRoleCreateEvent.EventObject> {
	protected GuildRoleUpdateEvent() {
		this.event_type = EventType.GUILD_ROLE_UPDATE;
	}
}
