package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.guild.Emoji;

/**
 * @author ShortCircuit908
 *         Created on 11/8/2016.
 */
public class MessageReactionRemoveEvent extends MessageEvent<MessageReactionAddEvent.EventObject> {
	protected MessageReactionRemoveEvent() {
		this.event_type = EventType.MESSAGE_REACTION_ADD;
	}
}
