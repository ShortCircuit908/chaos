package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Game;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GatewayStatusUpdate extends GatewayDispatch<GatewayStatusUpdate.EventObject> {
	public GatewayStatusUpdate() {
		this.opcode = Opcodes.STATUS_UPDATE.opcode;
	}

	public GatewayStatusUpdate(EventObject data) {
		this();
		this.data = data;
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected long idle_since;
		protected Game game;

		public long getIdleSince() {
			return idle_since;
		}

		public Game getGame() {
			return game;
		}
	}
}
