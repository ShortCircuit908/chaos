package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.guild.Guild;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GuildUpdateEvent extends GuildEvent<Guild> {
	protected GuildUpdateEvent() {
		this.event_type = EventType.GUILD_UPDATE;
	}
}
