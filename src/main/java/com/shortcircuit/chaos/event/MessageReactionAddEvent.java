package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.guild.Emoji;

/**
 * @author ShortCircuit908
 *         Created on 11/8/2016.
 */
public class MessageReactionAddEvent extends MessageEvent<MessageReactionAddEvent.EventObject> {
	protected MessageReactionAddEvent() {
		this.event_type = EventType.MESSAGE_REACTION_ADD;
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected Snowflake user_id;
		protected Snowflake message_id;
		protected Snowflake channel_id;
		protected Emoji emoji;

		protected EventObject() {

		}

		public Snowflake getUserId() {
			return user_id;
		}

		public Snowflake getMessageId() {
			return message_id;
		}

		public Snowflake getChannelId() {
			return channel_id;
		}

		public Emoji getEmoji() {
			return emoji;
		}
	}
}
