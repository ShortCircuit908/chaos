package com.shortcircuit.chaos.event;

import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.guild.Emoji;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GuildEmojiUpdateEvent extends GuildEvent<GuildEmojiUpdateEvent.EventObject> {
	protected GuildEmojiUpdateEvent() {
		this.event_type = EventType.GUILD_EMOJI_UPDATE;
	}

	public static class EventObject extends GatewayDispatch.EventObject {
		protected Snowflake guild_id;
		protected Emoji[] emojis;

		protected EventObject() {

		}

		public Snowflake getGuildId() {
			return guild_id;
		}

		public Emoji[] getEmojis() {
			return emojis;
		}
	}
}
