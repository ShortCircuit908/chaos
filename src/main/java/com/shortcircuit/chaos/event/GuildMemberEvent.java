package com.shortcircuit.chaos.event;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public abstract class GuildMemberEvent<T> extends GuildEvent<T> {
	protected GuildMemberEvent() {

	}
}
