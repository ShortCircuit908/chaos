package com.shortcircuit.chaos;

import com.shortcircuit.chaos.dispatch.Endpoint;
import com.shortcircuit.chaos.dispatch.LoginData;
import com.shortcircuit.chaos.dispatch.RequestDispatcher;
import com.shortcircuit.chaos.event.EventDispatcher;
import com.shortcircuit.chaos.event.EventHandler;
import com.shortcircuit.chaos.event.Exclude;
import com.shortcircuit.chaos.event.ReadyEvent;
import com.shortcircuit.chaos.obj.Token;
import com.shortcircuit.chaos.obj.channel.ChannelManager;
import com.shortcircuit.chaos.obj.guild.GuildManager;
import com.shortcircuit.chaos.obj.invite.InviteManager;
import com.shortcircuit.chaos.obj.user.User;
import com.shortcircuit.chaos.obj.user.UserManager;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

/**
 * @author ShortCircuit908
 *         Created on 5/31/2016.
 */
@Exclude
public class DiscordClient {
	private static final AtomicLong client_num = new AtomicLong(0);
	private final long instance_number = client_num.getAndIncrement();
	private final Logger logger = Utils.newLogger("DiscordClient-" + instance_number);
	private final EventDispatcher event_dispatcher = new EventDispatcher(this);
	private final ChannelManager channel_manager = new ChannelManager(this);
	private final GuildManager guild_manager = new GuildManager(this);
	private final UserManager user_manager = new UserManager(this);
	private final InviteManager invite_manager = new InviteManager(this);
	private final DiscordConnection connection;
	private RequestDispatcher request_dispatcher;
	private User self;
	private boolean compress;
	private int large_threshold = 250;
	private int[] shard;

	public DiscordClient(Token token) {
		this(token, false);
	}

	public DiscordClient(Token token, boolean daemon) {
		this(token, 3, daemon);
	}

	public DiscordClient(Token token, int max_missed_heartbeats, boolean daemon) {
		this.connection = new DiscordConnection(this, max_missed_heartbeats, daemon);
		setToken(token);
		this.event_dispatcher.registerListener(this);
		this.event_dispatcher.registerListener(this.connection);
	}

	public static Token login(String email, Token password) throws IOException {
		LoginData data = new LoginData.Builder()
				.withEmail(email)
				.withPassword(password)
				.build();
		return RequestDispatcher.GLOBAL.dispatch(Endpoint.LOGIN, data);
	}

	public void logout() throws IOException {
		request_dispatcher.dispatch(Endpoint.LOGOUT);
	}

	public long getInstanceNumber() {
		return instance_number;
	}

	public User getSelf() {
		return self;
	}

	public Logger getLogger() {
		return logger;
	}

	public int[] getShard() {
		return shard;
	}

	public DiscordClient setShard(int[] shard) {
		this.shard = shard;
		return this;
	}

	public boolean allowsCompression() {
		return compress;
	}

	public DiscordClient setAllowCompression(boolean compress) {
		this.compress = compress;
		return this;
	}

	public int getLargeThreshold() {
		return large_threshold;
	}

	public DiscordClient setLargeThreshold(int large_threshold) {
		this.large_threshold = large_threshold;
		return this;
	}

	public DiscordClient defaultLargeThreshold() {
		return setLargeThreshold(250);
	}

	@EventHandler
	private void onReady(final ReadyEvent event) {
		self = event.getData().getUser();
		request_dispatcher.setBot(self.isBot());
	}

	public DiscordClient setToken(Token token) {
		this.request_dispatcher = new RequestDispatcher(token, DiscordConstants.USER_AGENT);
		this.connection.setToken(token);
		return this;
	}

	public ChannelManager getChannelManager() {
		return channel_manager;
	}

	public GuildManager getGuildManager() {
		return guild_manager;
	}

	public UserManager getUserManager() {
		return user_manager;
	}

	public InviteManager getInviteManager() {
		return invite_manager;
	}

	public EventDispatcher getEventDispatcher() {
		return event_dispatcher;
	}

	public RequestDispatcher getRequestDispatcher() {
		return request_dispatcher;
	}

	public void connect() throws Exception {
		connection.connect();
	}

	public void disconnect(boolean complete) throws IOException {
		connection.disconnect(complete);
	}

	public DiscordConnection getConnection() {
		return connection;
	}
}
