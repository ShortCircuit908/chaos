package com.shortcircuit.chaos;

import com.shortcircuit.chaos.dispatch.Endpoint;
import com.shortcircuit.chaos.event.*;
import com.shortcircuit.chaos.exception.ClientShutDownException;
import com.shortcircuit.chaos.exception.ConnectedException;
import com.shortcircuit.chaos.exception.DiscordException;
import com.shortcircuit.chaos.exception.NotConnectedException;
import com.shortcircuit.chaos.obj.Gateway;
import com.shortcircuit.chaos.obj.Token;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.websocket.api.*;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.channels.UnresolvedAddressException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

/**
 * @author ShortCircuit908
 *         Created on 9/13/2016.
 */
@SuppressWarnings("unused")
public class DiscordConnection implements WebSocketListener, WebSocketConnectionListener, WebSocketPingPongListener {
	private final DiscordClient client;
	private final Timer gateway_timer;
	private Session session;
	private UUID session_id;
	private WebSocketClient socket;
	private boolean daemon;
	private String gateway_url;
	private int max_missed_pings;
	private int missed_pings;
	private boolean pong_received = true;
	private Token token;
	private final AtomicInteger last_seq = new AtomicInteger(0);
	private TimerTask heartbeat_task;
	private TimerTask reconnect_task;
	private TimerTask ping_pong_task;
	private final AtomicBoolean connected = new AtomicBoolean(false);
	private final AtomicBoolean ready = new AtomicBoolean(false);
	private final AtomicBoolean reconnecting = new AtomicBoolean(false);
	private final AtomicBoolean shutdown = new AtomicBoolean(false);


	protected DiscordConnection(DiscordClient client, int max_missed_pings, boolean daemon) {
		this.client = client;
		this.max_missed_pings = max_missed_pings;
		this.daemon = daemon;
		gateway_timer = new Timer("GatewayTimer", daemon);
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				if (connected.get()) {
					try {
						disconnect(true);
					}
					catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}, "DiscordConnection Shutdown Hook"));
	}

	public DiscordConnection setToken(Token token) {
		this.token = token;
		return this;
	}

	private void newReconnectTask() {
		if (reconnect_task != null) {
			reconnect_task.cancel();
			gateway_timer.purge();
		}
		reconnecting.set(true);
		reconnect_task = new TimerTask() {
			@Override
			public void run() {
				if (reconnecting.get()) {
					try {
						disconnect(false, true);
						getGatewayUrl(true);
						connect();
					}
					catch (Exception e) {
						client.getLogger().info("Reconnect failed: " + e.getClass().getName() + ": " + e.getMessage());
					}
				}
				else {
					cancel();
					gateway_timer.purge();
					reconnect_task = null;
				}
			}
		};
		gateway_timer.schedule(reconnect_task, 0, 5000);
	}

	private void newPingPongTask() {
		if (ping_pong_task != null) {
			ping_pong_task.cancel();
			gateway_timer.purge();
		}
		ping_pong_task = new TimerTask() {
			@Override
			public void run() {
				if (connected.get()) {
					try {
						if (!pong_received) {
							missed_pings++;
							client.getLogger().info("Server missed ping ("
									+ missed_pings + "/"
									+ max_missed_pings + ")"
							);
						}
						if (missed_pings >= max_missed_pings) {
							if (!reconnecting.get()) {
								newReconnectTask();
							}
							cancel();
							gateway_timer.purge();
							ping_pong_task = null;
							return;
						}
						pong_received = false;
						session.getRemote().sendPing(ByteBuffer.wrap("Are you there, server? It's me, Margaret.".getBytes("UTF-8")));
					}
					catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		};
		gateway_timer.schedule(ping_pong_task, 0, 3000);
	}

	private void newHeartbeatTask(long heartbeat_interval) {
		if (heartbeat_task != null) {
			heartbeat_task.cancel();
			gateway_timer.purge();
		}
		heartbeat_task = new TimerTask() {
			@Override
			public void run() {
				try {
					sendPacket(new GatewayHeartbeat(last_seq.get()));
				}
				catch (IOException | WebSocketException e) {
					// Do nothing
				}
			}
		};
		gateway_timer.schedule(heartbeat_task, heartbeat_interval, heartbeat_interval);
	}

	private String getGatewayUrl(boolean force_fetch) throws IOException {
		if (gateway_url != null && !force_fetch) {
			return gateway_url;
		}
		Gateway gateway = client.getRequestDispatcher().dispatch(Endpoint.GET_GATEWAY);
		return (gateway_url = gateway.getUrl() + "?encoding=json&v=5");
	}

	@EventHandler
	private void onHeartbeatAcknowledge(final GatewayHeartbeatAcknowledge event) {

	}

	@EventHandler
	private void generalEvent(final GatewayDispatch event) {
		if (event.getSequence() > 0) {
			last_seq.set(event.getSequence());
		}
	}

	@EventHandler
	private void onReady(final ReadyEvent event) {
		session_id = event.getData().getSessionId();
		ready.set(true);
		client.getLogger().info("Ready!");
	}

	@EventHandler
	private void onHello(final GatewayHello event) {
		missed_pings = 0;
		pong_received = true;
		newHeartbeatTask(event.getData().getHeartbeatInterval());
		newPingPongTask();
		connected.set(true);
		client.getLogger().info("Handshake complete");
		if (session_id != null) {
			client.getLogger().info("Resuming session...");
			GatewayResume packet = new GatewayResume(
					new GatewayResume.EventObject(token, session_id, last_seq.get()));
			try {
				sendPacket(packet, true);
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@EventHandler
	private void onSuggestReconnect(final GatewayReconnect event) {
		newReconnectTask();
	}

	@EventHandler
	private void onResume(final ResumeEvent event) {
		if (reconnecting.get()) {
			reconnecting.set(false);
			client.getLogger().info("Reconnected");
		}
	}

	@EventHandler
	public void onResumeReject(final GatewayInvalidSession event) {
		client.getLogger().info("Server rejected session resume");
		try {
			disconnect(false);
			connect();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void receive(String json) throws DiscordException {
		client.getEventDispatcher().dispatchEvent(GatewayDispatch.fromJson(json));
	}

	public void receive(byte[] payload) throws IOException {
		receive(payload, 0, payload.length);
	}

	public void receive(byte[] payload, int offset, int len) throws IOException {
		InputStream in = new ByteArrayInputStream(payload, offset, len);
		if (client.allowsCompression()) {
			in = new InflaterInputStream(in);
		}
		byte[] buffer = new byte[512];
		StringBuilder builder = new StringBuilder();
		int read;
		while ((read = in.read(buffer)) > 0) {
			builder.append(new String(buffer, 0, read));
		}
		in.close();
		receive(builder.toString());
	}

	@Override
	public void onWebSocketBinary(byte[] payload, int offset, int len) {
		try {
			receive(payload, offset, len);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onWebSocketText(String message) {
		try {
			receive(message);
		}
		catch (DiscordException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onWebSocketClose(int status_code, String reason) {
		client.getLogger().info("Web socket closed: " + status_code + ": " + reason);
	}

	@Override
	public void onWebSocketConnect(Session session) {
		this.session = session;
		client.getLogger().info("Web socket opened");
		try {
			if (session_id == null) {
				startHandshake();
			}
			else {
				sendPacket(new GatewayResume(new GatewayResume.EventObject(token, session_id, last_seq.get())), true);
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onWebSocketError(Throwable cause) {
		if (!(reconnecting.get() && cause instanceof UnresolvedAddressException)) {
			cause.printStackTrace();
		}
	}

	public void connect() throws Exception {
		if (shutdown.get()) {
			throw new ClientShutDownException();
		}
		if (connected.get()) {
			throw new ConnectedException();
		}
		client.getLogger().info("Connecting...");

		getGatewayUrl(false);

		if (socket == null || socket.isStopped()) {
			SslContextFactory ssl_factory = new SslContextFactory();
			socket = new WebSocketClient(ssl_factory);
			socket.setDaemon(daemon);
			socket.setConnectTimeout(3000);
			socket.setAsyncWriteTimeout(3000);
			socket.start();
		}

		ClientUpgradeRequest upgrade_request = new ClientUpgradeRequest();
		upgrade_request.setHeader("Accept-Encoding", "gzip, deflate");
		session = socket.connect(this, new URI(gateway_url), upgrade_request).get(3, TimeUnit.SECONDS);
	}

	public void sendPacket(GatewayDispatch<?> packet) throws IOException {
		sendPacket(packet, false);
	}

	protected void sendPacket(GatewayDispatch<?> packet, boolean force) throws IOException {
		if (shutdown.get()) {
			throw new ClientShutDownException();
		}
		if (!ready.get() && !force) {
			throw new NotConnectedException();
		}
		String json = packet.toJson();
		if (client.allowsCompression() && packet.getOpcode() == 0) {
			byte[] raw_data = json.getBytes();
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			DeflaterOutputStream dout = new DeflaterOutputStream(bout);
			dout.write(raw_data);
			dout.flush();
			dout.close();
			ByteBuffer buffer = ByteBuffer.wrap(bout.toByteArray());
			bout.close();
			session.getRemote().sendBytes(buffer);
		}
		else {
			session.getRemote().sendString(json);
		}
	}

	private void startHandshake() throws IOException {
		client.getLogger().info("Initiating handshake");
		GatewayIdentify.ClientProperties properties = new GatewayIdentify.ClientProperties(System.getProperty("os.name"),
				"Java", "Java", "", "");
		GatewayIdentify.EventObject data = new GatewayIdentify.EventObject(token, properties,
				client.allowsCompression(), client.getLargeThreshold(), client.getShard());
		sendPacket(new GatewayIdentify(data), true);
	}

	public void disconnect(boolean complete) throws IOException {
		disconnect(complete, false);
	}

	private void disconnect(boolean complete, boolean reconnecting) throws IOException {
		if (heartbeat_task != null) {
			heartbeat_task.cancel();
			heartbeat_task = null;
		}
		if (ping_pong_task != null) {
			ping_pong_task.cancel();
			ping_pong_task = null;
		}
		missed_pings = 0;
		pong_received = true;
		if (!reconnecting) {
			if (reconnect_task != null) {
				reconnect_task.cancel();
				reconnect_task = null;
			}
			session_id = null;
		}
		gateway_timer.purge();
		if (!connected.get()) {
			if (reconnecting || complete) {
				return;
			}
			throw new NotConnectedException();
		}
		client.getLogger().info("Disconnecting...");
		ready.set(false);
		connected.set(false);
		session.disconnect();
		session.close();
		client.getLogger().info("Disconnected");
		if (complete) {
			shutdown.set(true);
			client.getLogger().info("Shutting down...");
			gateway_timer.cancel();
			token.nullOut();
			token = null;
			System.gc();
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						socket.stop();
					}
					catch (Exception e) {
						e.printStackTrace();
					}
					client.getLogger().info("Shutdown complete");
				}
			}, "WebSocket Shutdown").start();
		}
	}

	@Override
	public void onWebSocketPing(ByteBuffer payload) {

	}

	@Override
	public void onWebSocketPong(ByteBuffer payload) {
		pong_received = true;
		missed_pings = 0;
	}

	public boolean isDaemon() {
		return daemon;
	}

	public boolean isConnected() {
		return connected.get();
	}

	public boolean isReady() {
		return ready.get();
	}

	public boolean isShutdown() {
		return shutdown.get();
	}

	public boolean isReconnecting() {
		return reconnecting.get();
	}

	public UUID getSessionId() {
		return session_id;
	}
}
