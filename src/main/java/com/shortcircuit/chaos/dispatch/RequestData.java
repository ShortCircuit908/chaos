package com.shortcircuit.chaos.dispatch;

import org.apache.http.HttpEntity;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/26/2016.
 */
public interface RequestData {

	HttpEntity toEntity() throws IOException;

	abstract class Builder<T extends RequestData> {
		public Builder() {
		}

		public abstract T build() throws IllegalArgumentException;
	}
}
