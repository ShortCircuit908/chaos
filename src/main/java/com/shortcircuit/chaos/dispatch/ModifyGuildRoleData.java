package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.awt.*;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class ModifyGuildRoleData implements RequestData {
	private String id;
	private String name;
	private int permissions;
	private int position;
	private int color;
	private boolean hoist;

	protected ModifyGuildRoleData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<ModifyGuildRoleData> {
		protected String id;
		protected String name;
		protected Integer permissions;
		protected Integer position;
		protected Integer color;
		protected Boolean hoist;

		public Builder withId(String id) {
			this.id = id;
			return this;
		}

		public Builder withoutId() {
			this.id = null;
			return this;
		}

		public Builder withName(String name) {
			this.name = name;
			return this;
		}

		public Builder withoutName() {
			this.name = null;
			return this;
		}

		public Builder setPermissions(int... permissions) {
			this.permissions = 0;
			addPermissions(permissions);
			return this;
		}

		public Builder addPermissions(int... permissions) {
			for (int permission : permissions) {
				this.permissions |= permission;
			}
			return this;
		}

		public Builder removePermissions(int... permissions) {
			for (int permission : permissions) {
				this.permissions &= ~permission;
			}
			return this;
		}

		public Builder withoutPermissions() {
			this.permissions = null;
			return this;
		}

		public Builder withPosition(int position) {
			this.position = position;
			return this;
		}

		public Builder withoutPosition() {
			this.position = null;
			return this;
		}

		public Builder withColor(Color color) {
			return withColor(color.getRGB());
		}

		public Builder withColor(int color) {
			this.color = color;
			return this;
		}

		public Builder withoutColor() {
			this.color = null;
			return this;
		}

		public Builder withHoist(boolean hoist) {
			this.hoist = hoist;
			return this;
		}

		public Builder withoutHoist() {
			this.hoist = null;
			return this;
		}

		@Override
		public ModifyGuildRoleData build() {
			ModifyGuildRoleData data = new ModifyGuildRoleData();
			data.id = id;
			data.name = name;
			data.permissions = permissions;
			data.position = position;
			data.color = color;
			data.hoist = hoist;
			return data;
		}
	}
}
