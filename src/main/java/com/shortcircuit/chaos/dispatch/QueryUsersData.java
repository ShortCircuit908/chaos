package com.shortcircuit.chaos.dispatch;

import org.apache.http.message.BasicNameValuePair;

import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class QueryUsersData implements URLParamRequestData {
	private String query;
	private Integer limit;

	protected QueryUsersData() {

	}

	@Override
	public LinkedList<BasicNameValuePair> getQueryParameters() {
		LinkedList<BasicNameValuePair> pairings = new LinkedList<>();
		pairings.add(new BasicNameValuePair("q", query));
		if (limit != null) {
			pairings.add(new BasicNameValuePair("limit", limit + ""));
		}
		return pairings;
	}

	public static class Builder extends RequestData.Builder<QueryUsersData> {
		protected String query;
		protected Integer limit;

		public Builder withQuery(String query) {
			if (query == null) {
				throw new IllegalArgumentException("query cannot be null");
			}
			this.query = query;
			return this;
		}

		public Builder withLimit(int limit) {
			this.limit = limit;
			return this;
		}

		public Builder withoutLimit() {
			this.limit = null;
			return this;
		}

		@Override
		public QueryUsersData build() throws IllegalArgumentException {
			withQuery(query);
			QueryUsersData data = new QueryUsersData();
			data.query = query;
			data.limit = limit;
			return data;
		}
	}
}
