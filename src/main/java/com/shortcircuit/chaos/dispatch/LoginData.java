package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Token;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class LoginData implements RequestData {
	private String email;
	private Token password;

	protected LoginData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<LoginData> {
		protected String email;
		protected Token password;

		public Builder withEmail(String email) {
			if (email == null) {
				throw new IllegalArgumentException("email cannot be null");
			}
			this.email = email;
			return this;
		}

		public Builder withPassword(Token password) {
			if (password == null || password.getToken() == null) {
				throw new IllegalArgumentException("password cannot be null");
			}
			this.password = password;
			return this;
		}

		@Override
		public LoginData build() throws IllegalArgumentException {
			withEmail(email);
			withPassword(password);
			LoginData data = new LoginData();
			data.email = email;
			data.password = password;
			return data;
		}
	}
}
