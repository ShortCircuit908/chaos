package com.shortcircuit.chaos.dispatch;

import com.google.gson.JsonSyntaxException;
import com.shortcircuit.chaos.DiscordConstants;
import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.exception.DiscordException;
import com.shortcircuit.chaos.obj.Token;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * @author ShortCircuit908
 *         Created on 5/26/2016.
 */
public class RequestDispatcher {
	public static final RequestDispatcher GLOBAL = new RequestDispatcher(DiscordConstants.USER_AGENT);
	private final String user_agent;
	private final Token token;
	private final CloseableHttpClient client;
	private boolean bot;

	public RequestDispatcher(String user_agent) {
		this(null, user_agent);
	}

	public RequestDispatcher(Token token, String user_agent) {
		this.token = token;
		this.user_agent = user_agent;
		this.client = HttpClients.custom().setUserAgent(user_agent).build();
	}

	public void setBot(boolean bot) {
		this.bot = bot;
	}

	public <T> T dispatch(Endpoint endpoint, String... endpoint_args) throws IOException {
		return dispatch(endpoint, null, endpoint_args);
	}

	public <T> T dispatch(Endpoint endpoint, RequestData data, String... endpoint_args) throws IOException {
		if (endpoint.data_type != null) {
			if (data == null || !data.getClass().equals(endpoint.data_type)) {
				throw new IllegalArgumentException(endpoint.data_type + " required");
			}
		}
		String formatted_endpoint = String.format(endpoint.endpoint, (Object[]) endpoint_args);
		if (data instanceof URLParamRequestData) {
			formatted_endpoint += "?" + URLEncodedUtils.format(((URLParamRequestData) data).getQueryParameters(), Charset.forName("UTF-8"));
		}
		HttpUriRequest base = null;
		switch (endpoint.method.toUpperCase()) {
			case HttpPost.METHOD_NAME:
				base = new HttpPost(formatted_endpoint);
				break;
			case HttpGet.METHOD_NAME:
				base = new HttpGet(formatted_endpoint);
				break;
			case HttpDelete.METHOD_NAME:
				base = new HttpDelete(formatted_endpoint);
				break;
			case HttpPatch.METHOD_NAME:
				base = new HttpPatch(formatted_endpoint);
				break;
			case HttpPut.METHOD_NAME:
				base = new HttpPut(formatted_endpoint);
				break;
		}
		if (base == null) {
			throw new IOException("Unknown request method: " + endpoint.method);
		}
		if (user_agent != null) {
			base.addHeader("User-Agent", user_agent);
		}
		if (token != null) {
			base.addHeader("Authorization", (bot ? "Bot " : "") + new String(token.getToken()));
		}
		if (endpoint.content_type != null) {
			base.addHeader("Content-Type", endpoint.content_type);
		}
		if (base instanceof HttpEntityEnclosingRequestBase && data != null && !(data instanceof URLParamRequestData)) {
			((HttpEntityEnclosingRequestBase) base).setEntity(data.toEntity());
		}
		CloseableHttpResponse response = client.execute(base);
		T obj = null;
		if (response.getEntity() != null) {
			String message = EntityUtils.toString(response.getEntity());
			try {
				DiscordException exception = GatewayDispatch.GSON.fromJson(message, DiscordException.class);
				if (exception != null && exception.getCode() >= 0) {
					throw exception;
				}
			}
			catch (JsonSyntaxException e) {
				// Do nothing
			}
			try {
				if (endpoint.return_type != null) {
					obj = GatewayDispatch.GSON.fromJson(message, endpoint.return_type);
				}
			}
			catch (Exception e) {
				try {
					throw GatewayDispatch.GSON.fromJson(message, DiscordException.class);
				}
				catch (Exception e1) {
					throw e;
				}
			}
		}
		response.close();
		return obj;
	}
}
