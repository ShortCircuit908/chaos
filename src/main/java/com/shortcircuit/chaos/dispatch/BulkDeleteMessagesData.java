package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 5/26/2016.
 */
public class BulkDeleteMessagesData implements RequestData {
	private String[] messages;

	protected BulkDeleteMessagesData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<BulkDeleteMessagesData> {
		protected LinkedList<String> messages = new LinkedList<>();

		public Builder setMessages(String... message_ids) {
			messages.clear();
			return addMessages(message_ids);
		}

		public Builder setMessages(Collection<? extends String> message_ids) {
			messages.clear();
			return addMessages(message_ids);
		}

		public Builder addMessages(String... message_ids) {
			Collections.addAll(messages, message_ids);
			return this;
		}

		public Builder addMessages(Collection<? extends String> message_ids) {
			messages.addAll(message_ids);
			return this;
		}

		public Builder removeMessages(String... message_ids) {
			for (String message_id : message_ids) {
				messages.remove(message_id);
			}
			return this;
		}

		public Builder removeMessages(Collection<? extends String> message_ids) {
			messages.removeAll(message_ids);
			return this;
		}

		@Override
		public BulkDeleteMessagesData build() throws IllegalArgumentException {
			if (messages.size() < 2) {
				throw new IllegalArgumentException("too few messages (min 2)");
			}
			if (messages.size() > 100) {
				throw new IllegalArgumentException("too many messages (max 100)");
			}
			BulkDeleteMessagesData data = new BulkDeleteMessagesData();
			data.messages = messages.toArray(new String[0]);
			return data;
		}
	}
}
