package com.shortcircuit.chaos.dispatch;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class ListGuildMembersData implements URLParamRequestData {
	private Integer limit;
	private Integer offset;

	protected ListGuildMembersData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		StringBuilder builder = new StringBuilder();
		if (limit != null) {
			builder.append("limit=").append(limit);
		}
		if (offset != null) {
			if (limit != null) {
				builder.append('&');
			}
			builder.append("offset=").append(offset);
		}
		return new StringEntity(builder.toString());
	}

	@Override
	public LinkedList<BasicNameValuePair> getQueryParameters() {
		LinkedList<BasicNameValuePair> pairings = new LinkedList<>();
		if (limit != null) {
			pairings.add(new BasicNameValuePair("limit", limit + ""));
		}
		if (offset != null) {
			pairings.add(new BasicNameValuePair("offset", offset + ""));
		}
		return pairings;
	}

	public static class Builder extends RequestData.Builder<ListGuildMembersData> {
		protected Integer limit;
		protected Integer offset;

		public Builder withLimit(int limit) {
			if (limit < 1) {
				throw new IllegalArgumentException("limit is too low (min 1)");
			}
			if (limit > 1000) {
				throw new IllegalArgumentException("limit is too high (max 1000)");
			}
			this.limit = limit;
			return this;
		}

		public Builder withoutLimit() {
			this.limit = null;
			return this;
		}

		public Builder withOffset(int offset) {
			this.offset = offset;
			return this;
		}

		public Builder withoutOffset() {
			this.offset = null;
			return this;
		}

		@Override
		public ListGuildMembersData build() throws IllegalArgumentException {
			if (limit != null) {
				withLimit(limit);
			}
			ListGuildMembersData data = new ListGuildMembersData();
			data.limit = limit;
			data.offset = offset;
			return data;
		}
	}
}
