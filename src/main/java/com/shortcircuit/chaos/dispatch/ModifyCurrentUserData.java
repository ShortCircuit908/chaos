package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.Utils;
import com.shortcircuit.chaos.event.GatewayDispatch;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.awt.image.RenderedImage;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class ModifyCurrentUserData implements RequestData {
	private String username;
	private String avatar;

	protected ModifyCurrentUserData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<ModifyCurrentUserData> {
		protected String username;
		protected RenderedImage avatar;

		public Builder withUsername(String username) {
			this.username = username;
			return this;
		}

		@Override
		public ModifyCurrentUserData build() throws IllegalArgumentException {
			ModifyCurrentUserData data = new ModifyCurrentUserData();
			data.username = username;
			if (avatar != null) {
				try {
					data.avatar = Utils.imageToDataUrl(avatar, "jpeg");
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
			return data;
		}
	}
}
