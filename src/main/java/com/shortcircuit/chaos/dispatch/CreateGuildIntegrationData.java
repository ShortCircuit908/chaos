package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class CreateGuildIntegrationData implements RequestData {
	private String type;
	private String id;

	protected CreateGuildIntegrationData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<CreateGuildIntegrationData> {
		protected String type;
		protected String id;

		public Builder withType(String type) {
			if (type == null) {
				throw new IllegalArgumentException("type cannot be null");
			}
			this.type = type;
			return this;
		}

		public Builder withIntegrationId(String id) {
			if (id == null) {
				throw new IllegalArgumentException("id cannot be null");
			}
			this.id = id;
			return this;
		}

		@Override
		public CreateGuildIntegrationData build() throws IllegalArgumentException {
			withType(type);
			withIntegrationId(id);
			CreateGuildIntegrationData data = new CreateGuildIntegrationData();
			data.id = id;
			data.type = type;
			return data;
		}
	}
}
