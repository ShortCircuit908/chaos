package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/26/2016.
 */
public class CreateMessageData extends EditMessageData {
	protected String nonce;
	protected Boolean tts;

	protected CreateMessageData() {

	}

	public static class Builder extends EditMessageData.Builder {
		protected String nonce;
		protected Boolean tts;

		public Builder withNonce(String nonce) {
			this.nonce = nonce;
			return this;
		}

		public Builder withoutNonce() {
			return withNonce(null);
		}

		public Builder withTTS(boolean tts) {
			this.tts = tts;
			return this;
		}

		public Builder withoutTTS() {
			this.tts = null;
			return this;
		}

		@Override
		public Builder appendContent(String content, Style... styles) {
			return (Builder) super.appendContent(content, styles);
		}

		@Override
		public Builder withContent(String content, Style... styles) {
			return (Builder) super.withContent(content, styles);
		}

		@Override
		public Builder appendCode(String language, String content, Style... styles) {
			return (Builder) super.appendCode(language, content, styles);
		}

		@Override
		public Builder withCode(String language, String content, Style... styles) {
			return (Builder) super.withCode(language, content, styles);
		}

		@Override
		public Builder appendMention(Mention mention, Snowflake snowflake, Style... styles) {
			return (Builder) super.appendMention(mention, snowflake, styles);
		}

		@Override
		public Builder withMention(Mention mention, Snowflake snowflake, Style... styles) {
			return (Builder) super.withMention(mention, snowflake, styles);
		}

		@Override
		public CreateMessageData build() throws IllegalArgumentException {
			withContent(content.toString());
			CreateMessageData data = new CreateMessageData();
			data.content = content.toString();
			data.nonce = nonce;
			data.tts = tts;
			return data;
		}
	}
}
