package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/26/2016.
 */
public class EditChannelPermissionsData implements RequestData {
	private int allow;
	private int deny;

	protected EditChannelPermissionsData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<EditChannelPermissionsData> {
		protected int allow;
		protected int deny;

		public Builder addAllowPermissions(int... permissions) {
			for (int permission : permissions) {
				this.allow |= permission;
			}
			return this;
		}

		public Builder addDenyPermissions(int... permissions) {
			for (int permission : permissions) {
				this.deny |= permission;
			}
			return this;
		}

		public Builder removeAllowPermissions(int... permissions) {
			for (int permission : permissions) {
				this.allow &= ~permission;
			}
			return this;
		}

		public Builder removeDenyPermissions(int... permissions) {
			for (int permission : permissions) {
				this.deny &= ~permission;
			}
			return this;
		}

		public Builder setAllowPermissions(int... permissions) {
			this.allow = 0;
			addAllowPermissions(permissions);
			return this;
		}

		public Builder setDenyPermissions(int... permissions) {
			this.deny = 0;
			addDenyPermissions(permissions);
			return this;
		}

		@Override
		public EditChannelPermissionsData build() {
			EditChannelPermissionsData data = new EditChannelPermissionsData();
			data.allow = allow;
			data.deny = deny;
			return data;
		}
	}
}
