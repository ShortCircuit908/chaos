package com.shortcircuit.chaos.dispatch;

import com.google.gson.annotations.SerializedName;
import com.shortcircuit.chaos.event.GatewayDispatch;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class CreateGuildBanData implements RequestData {
	@SerializedName("delete-message-days")
	private Integer delete_message_days;

	protected CreateGuildBanData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<CreateGuildBanData> {
		protected Integer delete_message_days;

		public Builder withDeletePastMessages(int delete_message_days) {
			if (delete_message_days > 7) {
				throw new IllegalArgumentException("delete_message_days is too high (max 7)");
			}
			this.delete_message_days = delete_message_days;
			return this;
		}

		public Builder withoutDeletePastMessages() {
			this.delete_message_days = null;
			return this;
		}

		@Override
		public CreateGuildBanData build() {
			if (delete_message_days != null) {
				withDeletePastMessages(delete_message_days);
			}
			CreateGuildBanData data = new CreateGuildBanData();
			data.delete_message_days = delete_message_days;
			return data;
		}
	}
}
