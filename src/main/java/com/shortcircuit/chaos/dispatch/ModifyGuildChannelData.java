package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class ModifyGuildChannelData implements RequestData {
	private String id;
	private int position;

	protected ModifyGuildChannelData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<ModifyGuildChannelData> {
		protected String id;
		protected int position;

		public Builder withChannelId(String id) {
			if (id == null) {
				throw new IllegalArgumentException("id cannot be null");
			}
			this.id = id;
			return this;
		}

		public Builder withPosition(int position) {
			this.position = position;
			return this;
		}

		@Override
		public ModifyGuildChannelData build() throws IllegalArgumentException {
			withChannelId(id);
			ModifyGuildChannelData data = new ModifyGuildChannelData();
			data.id = id;
			data.position = position;
			return data;
		}
	}
}
