package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/26/2016.
 */
public class CreateChannelInviteData implements RequestData {
	private Integer max_age;
	private Integer max_uses;
	private Boolean temporary;
	private Boolean xkcdpass;

	protected CreateChannelInviteData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		System.out.println(GatewayDispatch.GSON.toJson(this));
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<CreateChannelInviteData> {
		protected Integer max_age;
		protected Integer max_uses;
		protected Boolean temporary;
		protected Boolean xkcdpass;

		public Builder withMaxAge(int max_age) {
			this.max_age = max_age;
			return this;
		}

		public Builder withoutMaxAge() {
			this.max_age = null;
			return this;
		}

		public Builder withInfiniteAge() {
			this.max_age = 0;
			return this;
		}

		public Builder withMaxUses(int max_uses) {
			this.max_uses = max_uses;
			return this;
		}

		public Builder withInfiniteUses() {
			this.max_uses = 0;
			return this;
		}

		public Builder setTemporary(boolean temporary) {
			this.temporary = temporary;
			return this;
		}

		public Builder resetTemporary() {
			this.temporary = null;
			return this;
		}

		public Builder setXkcdpass(boolean xkcdpass) {
			this.xkcdpass = xkcdpass;
			return this;
		}

		public Builder resetXkcdpass() {
			this.xkcdpass = null;
			return this;
		}

		@Override
		public CreateChannelInviteData build() {
			CreateChannelInviteData data = new CreateChannelInviteData();
			data.max_age = max_age;
			data.max_uses = max_uses;
			data.temporary = temporary;
			data.xkcdpass = xkcdpass;
			return data;
		}
	}
}
