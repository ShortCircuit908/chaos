package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/26/2016.
 */
public class ModifyChannelData implements RequestData {
	private String name;
	private Integer position;
	private String topic;
	private Integer bitrate;

	protected ModifyChannelData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<ModifyChannelData> {
		protected String name;
		protected Integer position;
		protected String topic;
		protected Integer bitrate;

		public Builder withName(String name) {
			if (name != null) {
				if (name.length() < 2) {
					throw new IllegalArgumentException("name is too short (min 2 characters)");
				}
				if (name.length() > 100) {
					throw new IllegalArgumentException("name is too long (max 100 characters)");
				}
			}
			this.name = name;
			return this;
		}

		public Builder withoutName() {
			this.name = null;
			return this;
		}

		public Builder withPosition(int position) {
			this.position = position;
			return this;
		}

		public Builder withoutPosition() {
			this.position = null;
			return this;
		}

		public Builder withTopic(String topic) {
			if (topic != null) {
				if (topic.length() > 1024) {
					throw new IllegalArgumentException("topic is too long (max 1024 characters)");
				}
			}
			this.topic = topic;
			return this;
		}

		public Builder withoutTopic() {
			this.topic = null;
			return this;
		}

		public Builder withBitrate(int bitrate) {
			if (bitrate < 8000) {
				throw new IllegalArgumentException("bitrate is too low (min 8000)");
			}
			if (bitrate > 120000) {
				throw new IllegalArgumentException("bitrate is too high (max 120000)");
			}
			this.bitrate = bitrate;
			return this;
		}

		public Builder withoutBitrate() {
			this.bitrate = null;
			return this;
		}

		@Override
		public ModifyChannelData build() throws IllegalArgumentException {
			withName(name);
			withTopic(topic);
			if (bitrate != null) {
				withBitrate(bitrate);
			}
			ModifyChannelData data = new ModifyChannelData();
			data.name = name;
			data.position = position;
			data.topic = topic;
			data.bitrate = bitrate;
			return data;
		}
	}
}
