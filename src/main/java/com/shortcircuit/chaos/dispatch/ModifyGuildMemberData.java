package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class ModifyGuildMemberData implements RequestData {
	private String nick;
	private String[] roles;
	private Boolean mute;
	private Boolean deaf;
	private String channel_id;

	protected ModifyGuildMemberData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<ModifyGuildMemberData> {
		protected String nick;
		protected LinkedList<String> roles;
		protected Boolean mute;
		protected Boolean deaf;
		protected String channel_id;

		public Builder withNick(String nick) {
			this.nick = nick;
			return this;
		}

		public Builder withoutNick() {
			this.nick = null;
			return this;
		}

		public Builder withRoles(String... roles) {
			if (this.roles == null) {
				this.roles = new LinkedList<>();
			}
			this.roles.clear();
			return addRoles(roles);
		}

		public Builder addRoles(String... roles) {
			if (this.roles == null) {
				this.roles = new LinkedList<>();
			}
			Collections.addAll(this.roles, roles);
			return this;
		}

		public Builder removeRoles(String... roles) {
			if (this.roles == null) {
				return this;
			}
			for (String role : roles) {
				this.roles.remove(role);
			}
			return this;
		}

		public Builder withMute(boolean mute) {
			this.mute = mute;
			return this;
		}

		public Builder withoutMute() {
			this.mute = null;
			return this;
		}

		public Builder withDeaf(boolean deaf) {
			this.deaf = deaf;
			return this;
		}

		public Builder withoutDeaf() {
			this.deaf = null;
			return this;
		}

		public Builder withChannelId(String channel_id) {
			this.channel_id = channel_id;
			return this;
		}

		public Builder withoutChannelId() {
			this.channel_id = null;
			return this;
		}

		@Override
		public ModifyGuildMemberData build() throws IllegalArgumentException {
			ModifyGuildMemberData data = new ModifyGuildMemberData();
			data.nick = nick;
			data.roles = roles.toArray(new String[0]);
			data.mute = mute;
			data.deaf = deaf;
			data.channel_id = channel_id;
			return data;
		}
	}
}
