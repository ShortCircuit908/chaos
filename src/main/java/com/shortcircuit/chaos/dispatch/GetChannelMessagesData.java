package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/26/2016.
 */
public class GetChannelMessagesData implements RequestData {
	private String before;
	private String after;
	private Integer limit;

	protected GetChannelMessagesData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<GetChannelMessagesData> {
		protected String before;
		protected String after;
		protected Integer limit;

		public Builder getBefore(String message_id) {
			this.before = message_id;
			this.after = null;
			return this;
		}

		public Builder getAfter(String message_id) {
			this.before = null;
			this.after = message_id;
			return this;
		}

		public Builder withLimit(int limit) {
			if (limit < 1) {
				throw new IllegalArgumentException("limit is too low (min 1)");
			}
			if (limit > 100) {
				throw new IllegalArgumentException("limit is too high (max 100)");
			}
			this.limit = limit;
			return this;
		}

		public Builder withoutLimit() {
			this.limit = null;
			return this;
		}

		@Override
		public GetChannelMessagesData build() throws IllegalArgumentException {
			if (limit != null) {
				withLimit(limit);
			}
			GetChannelMessagesData data = new GetChannelMessagesData();
			data.before = before;
			data.after = after;
			data.limit = limit;
			return data;
		}
	}
}
