package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/26/2016.
 */
public class EditMessageData implements RequestData {
	protected String content;

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public enum Mention {
		USER("<@%1$s>"),
		USER_NICKNAME("<@!%1$s>"),
		CHANNEL("<#%1$s>"),
		ROLE("<@&%1$s>");

		public final String format;

		Mention(String format) {
			this.format = format;
		}
	}

	public enum Style {
		ITALICS("*"),
		BOLD("**"),
		STRIKETHROUGH("~~"),
		MULTILINE_CODE("```"),
		INLINE_CODE("`"),
		UNDERLINE("__"),;
		public final String open_markdown;
		public final String close_markdown;

		Style(String open_markdown) {
			this.open_markdown = open_markdown;
			this.close_markdown = new StringBuilder(open_markdown).reverse().toString();
		}
	}

	public static class Builder extends RequestData.Builder<EditMessageData> {
		protected StringBuilder content = new StringBuilder();

		public Builder withContent(String content, Style... styles) {
			this.content = new StringBuilder();
			return appendContent(content, styles);
		}

		public Builder withCode(String language, String content, Style... styles) {
			this.content = new StringBuilder();
			return appendCode(language, content, styles);
		}

		public Builder appendCode(String language, String content, Style... styles) {
			Style[] vararg = new Style[styles.length + 1];
			System.arraycopy(styles, 0, vararg, 0, styles.length);
			vararg[vararg.length - 1] = Style.MULTILINE_CODE;
			return appendContent(language + "\n" + content, vararg);
		}

		public Builder appendContent(String content, Style... styles) {
			StringBuilder builder = new StringBuilder();
			if (styles.length > 0) {
				boolean[] multi_tracker = new boolean[Style.values().length];
				for (int i = 0; i < styles.length; i++) {
					if (styles[i] != null) {
						if (multi_tracker[styles[i].ordinal()]) {
							throw new IllegalArgumentException("Only one of each style is allowed");
						}
						multi_tracker[styles[i].ordinal()] = true;
						builder.append(styles[i].open_markdown);
					}
				}
			}
			builder.append(content);
			if (styles.length > 0) {
				for (int i = styles.length - 1; i >= 0; i--) {
					if (styles[i] != null) {
						builder.append(styles[i].close_markdown);
					}
				}
			}
			if (this.content.length() + builder.length() > 2000) {
				throw new IllegalArgumentException("content is too long (max 2000 characters)");
			}
			this.content.append(builder);
			return this;
		}

		public Builder appendMention(Mention mention, Snowflake snowflake, Style... styles) {
			return appendContent(String.format(mention.format, snowflake), styles);
		}

		public Builder withMention(Mention mention, Snowflake snowflake, Style... styles) {
			this.content = new StringBuilder();
			return appendMention(mention, snowflake, styles);
		}

		@Override
		public EditMessageData build() throws IllegalArgumentException {
			withContent(content.toString());
			EditMessageData data = new EditMessageData();
			data.content = content.toString();
			return data;
		}
	}
}
