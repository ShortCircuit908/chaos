package com.shortcircuit.chaos.dispatch;

import com.google.gson.reflect.TypeToken;
import com.shortcircuit.chaos.event.ChannelCreateEvent;
import com.shortcircuit.chaos.event.ChannelDeleteEvent;
import com.shortcircuit.chaos.obj.Gateway;
import com.shortcircuit.chaos.obj.PruneCount;
import com.shortcircuit.chaos.obj.Token;
import com.shortcircuit.chaos.obj.channel.DMChannel;
import com.shortcircuit.chaos.obj.channel.GuildChannel;
import com.shortcircuit.chaos.obj.channel.Message;
import com.shortcircuit.chaos.obj.guild.Guild;
import com.shortcircuit.chaos.obj.guild.GuildEmbed;
import com.shortcircuit.chaos.obj.guild.GuildMember;
import com.shortcircuit.chaos.obj.guild.Integration;
import com.shortcircuit.chaos.obj.invite.Invite;
import com.shortcircuit.chaos.obj.invite.InviteMetadata;
import com.shortcircuit.chaos.obj.permissions.Role;
import com.shortcircuit.chaos.obj.user.Connection;
import com.shortcircuit.chaos.obj.user.User;
import com.shortcircuit.chaos.obj.user.UserGuild;
import com.shortcircuit.chaos.obj.voice.VoiceRegion;

import java.lang.reflect.Type;
import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 5/26/2016.
 */
public enum Endpoint {
	// Miscellaneous
	GET_GATEWAY("GET", "gateway", Gateway.class),
	LOGIN("POST", "auth/login", "application/json", LoginData.class, Token.class),
	LOGOUT("POST", "auth/logout"),

	// Channel
	GET_CHANNEL("GET", "channels/%1$s", ChannelCreateEvent.EventObject.class),
	MODIFY_CHANNEL("PATCH", "channels/%1$s", "application/json", ModifyChannelData.class, GuildChannel.class),
	DELETE_CHANNEL("DELETE", "channels/%1$s", ChannelDeleteEvent.EventObject.class),
	GET_CHANNEL_MESSAGES("GET", "channels/%1$s/messages", "application/json", GetChannelMessagesData.class, new TypeToken<LinkedList<Message>>() {
	}.getType()),
	CREATE_MESSAGE("POST", "channels/%1$s/messages", "application/json", CreateMessageData.class, Message.class),
	UPLOAD_FILE("POST", "channels/%1$s/messages", "multipart/form-data", UploadFileData.class, Message.class),
	EDIT_MESSAGE("PATCH", "channels/%1$s/messages/%2$s", "application/json", EditMessageData.class, Message.class),
	DELETE_MESSAGE("DELETE", "channels/$1$s/messages/%2$s", Message.class),
	BULK_DELETE_MESSAGES("POST", "channels/%1$s/messages/bulk\\_delete", "application/json", BulkDeleteMessagesData.class),
	ACKNOWLEDGE_MESSAGE("POST", "channels/%1$s/messages/%2$s/ack"),
	EDIT_CHANNEL_PERMISSIONS("PUT", "channels/%1$s/permissions/%2$s", "application/json", EditChannelPermissionsData.class),
	GET_CHANNEL_INVITES("GET", "channels/%1$s/invites", new TypeToken<LinkedList<Invite>>() {
	}.getType()),
	CREATE_CHANNEL_INVITE("POST", "channels/%1$s/invites", "application/json", CreateChannelInviteData.class, InviteMetadata.class),
	DELETE_CHANNEL_PERMISSION("DELETE", "channels/$1$s/permissions/%2$s"),
	TRIGGER_TYPING_INDICATOR("POST", "channels/%1$s/typing"),
	GET_PINNED_MESSAGES("GET", "channels/%1$s/pins", new TypeToken<LinkedList<Message>>() {
	}.getType()),
	ADD_PINNED_CHANNEL_MESSAGE("PUT", "channels/%1$s/pins/%2$s"),
	DELETE_PINNED_CHANNEL_MESSAGE("DELETE", "channels/%1$s/pins/%2$s"),

	// Guild
	CREATE_GUILD("POST", "guilds", "application/json", CreateGuildData.class, Guild.class),
	GET_GUILD("GET", "guilds/%1$s", Guild.class),
	MODIFY_GUILD("PATCH", "guilds/%1$s", "application/json", ModifyGuildData.class, Guild.class),
	DELETE_GUILD("DELETE", "guilds/%1$s", Guild.class),
	GET_GUILD_CHANNELS("GET", "guilds/%1$s/channels", new TypeToken<LinkedList<GuildChannel>>() {
	}.getType()),
	CREATE_GUILD_CHANNEL("POST", "guilds/%1$s/channels", "application/json", CreateGuildChannelData.class, GuildChannel.class),
	MODIFY_GUILD_CHANNEL("PATCH", "guilds/%1$s", "application/json", ModifyGuildChannelData.class, GuildChannel.class),
	GET_GUILD_MEMBER("GET", "guilds/%1$s/members/%2$s", GuildMember.class),
	LIST_GUILD_MEMBERS("GET", "guilds/%1$s/members", "application/x-www-form-urlencoded", ListGuildMembersData.class, new TypeToken<LinkedList<GuildMember>>() {
	}.getType()),
	MODIFY_GUILD_MEMBER("PATCH", "guilds/%1$s/members/%2$s", "application/json", ModifyGuildMemberData.class, GuildMember.class),
	REMOVE_GUILD_MEMBER("DELETE", "guilds/%1$s/members/%2$s"),
	GET_GUILD_BANS("GET", "guilds/%1$s/bans", new TypeToken<LinkedList<User>>() {
	}.getType()),
	CREATE_GUILD_BAN("PUT", "guilds/%1$s/bans/%2$s", "application/json", CreateGuildBanData.class),
	REMOVE_GUILD_BAN("DELETE", "guilds/%1$s/bans/$2$s"),
	GET_GUILD_ROLES("GET", "guilds/%1$s/roles", new TypeToken<LinkedList<Role>>() {
	}.getType()),
	CREATE_GUILD_ROLE("POST", "guilds/%1$s/roles", Role.class),
	BATCH_MODIFY_GUILD_ROLE("PATCH", "guilds/%1$s/roles", "application/json", BatchModifyGuildRoleData.class, new TypeToken<LinkedList<Role>>() {
	}.getType()),
	MODIFY_GUILD_ROLE("PATCH", "guilds/%1$s/roles/$2$s", "application/json", ModifyGuildRoleData.class, Role.class),
	DELETE_GUILD_ROLE("DELETE", "guilds/%1$s/roles/%2$s", Role.class),
	GET_GUILD_PRUNE_COUNT("GET", "guilds/%1$s/prune", "application/json", GetGuildPruneCountData.class, PruneCount.class),
	BEGIN_GUILD_PRUNE("POST", "guilds/%1$s/prune", "application/json", BeginGuildPruneData.class, PruneCount.class),
	GET_GUILD_VOICE_REGIONS("GET", "guilds/%1$s/regions", new TypeToken<LinkedList<VoiceRegion>>() {
	}.getType()),
	GET_GUILD_INVITES("GET", "guilds/%1$s/invites", new TypeToken<LinkedList<InviteMetadata>>() {
	}.getType()),
	GET_GUILD_INTEGRATIONS("GET", "guilds/%1$s/integrations", new TypeToken<LinkedList<Integration>>() {
	}.getType()),
	CREATE_GUILD_INTEGRATION("POST", "guilds/%1$s/integrations", "application/json", CreateGuildIntegrationData.class),
	MODIFY_GUILD_INTEGRATION("PATCH", "guilds/%1$s/integrations/%2$s", "application/json", ModifyGuildIntegrationData.class),
	DELETE_GUILD_INTEGRATION("DELETE", "guilds/%1$s/integrations/%2$s"),
	SYNC_GUILD_INTEGRATION("POST", "guilds/%1$s/integrations/%2$s/sync"),
	GET_GUILD_EMBED("GET", "guilds/%1$s/embed", GuildEmbed.class),
	MODIFY_GUILD_EMBED("PATCH", "guilds/%1$s/embed", "application/json", ModifyGuildEmbedData.class, GuildEmbed.class),

	// User
	QUERY_USERS("GET", "users", "application/x-www-form-urlencoded", QueryUsersData.class, new TypeToken<LinkedList<User>>() {
	}.getType()),
	GET_CURRENT_USER("GET", "users/@me", User.class),
	GET_USER("GET", "users/%1$s", User.class),
	MODIFY_CURRENT_USER("PATCH", "users/@me", "application/json", ModifyCurrentUserData.class, User.class),
	GET_CURRENT_USER_GUILDS("GET", "users/@me/guilds", new TypeToken<LinkedList<UserGuild>>() {
	}.getType()),
	LEAVE_GUILD("DELETE", "users/@me/guilds/%1$s"),
	GET_USER_DMS("GET", "users/@me/channels", new TypeToken<LinkedList<DMChannel>>() {
	}.getType()),
	CREATE_DM("POST", "users/@me/channels", "application/json", CreateDMData.class, DMChannel.class),
	GET_USERS_CONNECTIONS("GET", "users/@me/connections", new TypeToken<LinkedList<Connection>>() {
	}.getType()),

	// Invite
	GET_INVITE("GET", "invites/%1$s", Invite.class),
	DELETE_INVITE("DELTE", "invites/%1$s", Invite.class),
	ACCEPT_INVITE("POST", "invites/%1$s", Invite.class)
	;

	public final String method;
	public final String endpoint;
	public final String content_type;
	public final Class<? extends RequestData> data_type;
	public final Type return_type;

	public final String BASE_URL = "https://discordapp.com/api/";

	Endpoint(String method, String endpoint) {
		this(method, endpoint, null, null, null);
	}

	Endpoint(String method, String endpoint, Type return_type) {
		this(method, endpoint, null, null, return_type);
	}

	Endpoint(String method, String endpoint, String content_type, Class<? extends RequestData> data_type) {
		this(method, endpoint, content_type, data_type, null);
	}

	Endpoint(String method, String endpoint, String content_type, Class<? extends RequestData> data_type, Type return_type) {
		this.method = method;
		this.endpoint = BASE_URL + endpoint;
		this.content_type = content_type;
		this.data_type = data_type;
		this.return_type = return_type;
	}
}
