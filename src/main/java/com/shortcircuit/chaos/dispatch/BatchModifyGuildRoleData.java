package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class BatchModifyGuildRoleData implements RequestData {
	private ModifyGuildRoleData[] batch;

	protected BatchModifyGuildRoleData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(batch));
	}

	public static class Builder extends RequestData.Builder<BatchModifyGuildRoleData> {
		protected LinkedList<ModifyGuildRoleData> batch = new LinkedList<>();

		public Builder setData(ModifyGuildRoleData... data) {
			batch.clear();
			return addData(data);
		}

		public Builder setData(Collection<? extends ModifyGuildRoleData> data) {
			batch.clear();
			return addData(data);
		}

		public Builder addData(ModifyGuildRoleData... data) {
			Collections.addAll(batch, data);
			return this;
		}

		public Builder addData(Collection<? extends ModifyGuildRoleData> data) {
			batch.addAll(data);
			return this;
		}

		public Builder removeData(ModifyGuildRoleData... data) {
			for (ModifyGuildRoleData part : data) {
				batch.remove(part);
			}
			return this;
		}

		public Builder removeData(Collection<? extends ModifyGuildRoleData> data) {
			batch.removeAll(data);
			return this;
		}

		@Override
		public BatchModifyGuildRoleData build() {
			BatchModifyGuildRoleData data = new BatchModifyGuildRoleData();
			data.batch = batch.toArray(new ModifyGuildRoleData[0]);
			return data;
		}
	}
}
