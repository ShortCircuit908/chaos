package com.shortcircuit.chaos.dispatch;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public interface URLParamRequestData extends RequestData {
	@Override
	default HttpEntity toEntity() throws IOException {
		return null;
	}

	Iterable<? extends NameValuePair> getQueryParameters();
}
