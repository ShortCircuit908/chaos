package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class CreateDMData implements RequestData {
	private Snowflake recipient_id;

	protected CreateDMData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<CreateDMData> {
		protected Snowflake recipient_id;

		public Builder withRecipientId(Snowflake recipient_id) {
			if (recipient_id == null) {
				throw new IllegalArgumentException("recipient_id cannot be null");
			}
			this.recipient_id = recipient_id;
			return this;
		}

		@Override
		public CreateDMData build() throws IllegalArgumentException {
			withRecipientId(recipient_id);
			CreateDMData data = new CreateDMData();
			data.recipient_id = recipient_id;
			return data;
		}
	}
}
