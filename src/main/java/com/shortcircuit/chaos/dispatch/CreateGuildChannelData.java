package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.channel.GuildChannel;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class CreateGuildChannelData implements RequestData {
	private String name;
	private GuildChannel.Type type;
	private Integer bitrate;

	protected CreateGuildChannelData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<CreateGuildChannelData> {
		protected String name;
		protected GuildChannel.Type type;
		protected Integer bitrate;

		public Builder withName(String name) {
			if (name == null) {
				throw new IllegalArgumentException("name cannot be null");
			}
			if (name.length() < 2) {
				throw new IllegalArgumentException("name is too short");
			}
			if (name.length() > 100) {
				throw new IllegalArgumentException("name is too long");
			}
			this.name = name;
			return this;
		}

		public Builder withType(GuildChannel.Type type) {
			if (type == null) {
				throw new IllegalArgumentException("type cannot be null");
			}
			this.type = type;
			return this;
		}

		public Builder withBitrate(int bitrate) {
			this.bitrate = bitrate;
			return this;
		}

		public Builder withoutBitrate() {
			this.bitrate = null;
			return this;
		}

		@Override
		public CreateGuildChannelData build() throws IllegalArgumentException {
			withName(name);
			withType(type);
			switch (type) {
				case TEXT:
					bitrate = null;
					break;
				case VOICE:
					withBitrate(bitrate);
					break;
			}
			CreateGuildChannelData data = new CreateGuildChannelData();
			data.name = name;
			data.type = type;
			data.bitrate = bitrate;
			return data;
		}
	}
}
