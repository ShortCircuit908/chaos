package com.shortcircuit.chaos.dispatch;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class BeginGuildPruneData extends GetGuildPruneCountData {
	protected BeginGuildPruneData() {

	}

	public static class Builder extends GetGuildPruneCountData.Builder {
		public Builder withDays(int days) throws IllegalArgumentException {
			return (Builder) super.withDays(days);
		}

		@Override
		public BeginGuildPruneData build() throws IllegalArgumentException {
			withDays(days);
			BeginGuildPruneData data = new BeginGuildPruneData();
			data.days = days;
			return data;
		}
	}
}
