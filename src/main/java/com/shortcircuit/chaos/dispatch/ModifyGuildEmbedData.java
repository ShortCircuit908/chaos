package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class ModifyGuildEmbedData implements RequestData {
	private Boolean enabled;
	private String channel_id;

	protected ModifyGuildEmbedData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<ModifyGuildEmbedData> {
		protected Boolean enabled;
		protected String channel_id;

		public Builder withEnabled(boolean enabled) {
			this.enabled = enabled;
			return this;
		}

		public Builder withoutEnabled() {
			this.enabled = null;
			return this;
		}

		public Builder withChannelId(String channel_id) {
			this.channel_id = channel_id;
			return this;
		}

		public Builder withoutChannelId() {
			this.channel_id = null;
			return this;
		}

		@Override
		public ModifyGuildEmbedData build() {
			ModifyGuildEmbedData data = new ModifyGuildEmbedData();
			data.channel_id = channel_id;
			data.enabled = enabled;
			return data;
		}
	}
}
