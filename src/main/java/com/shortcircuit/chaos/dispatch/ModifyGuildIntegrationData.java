package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class ModifyGuildIntegrationData implements RequestData {
	private Integer expire_behavior;
	private Integer expire_grace_period;
	private Boolean enable_emoticons;

	protected ModifyGuildIntegrationData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<ModifyGuildIntegrationData> {
		protected Integer expire_behavior;
		protected Integer expire_grace_period;
		protected Boolean enable_emoticons;

		public Builder withExpireBehavior(int expire_behavior) {
			this.expire_behavior = expire_behavior;
			return this;
		}

		public Builder withoutExpireBehavior() {
			this.expire_behavior = null;
			return this;
		}

		public Builder withExpireGracePeriod(int expire_grace_period) {
			this.expire_grace_period = expire_grace_period;
			return this;
		}

		public Builder withoutExpireGracePeriod() {
			this.expire_grace_period = null;
			return this;
		}

		public Builder withEnableEmoticons(boolean enable_emoticons) {
			this.enable_emoticons = enable_emoticons;
			return this;
		}

		public Builder withoutEnableEmoticons() {
			this.enable_emoticons = null;
			return this;
		}

		@Override
		public ModifyGuildIntegrationData build() throws IllegalArgumentException {
			ModifyGuildIntegrationData data = new ModifyGuildIntegrationData();
			data.enable_emoticons = enable_emoticons;
			data.expire_behavior = expire_behavior;
			data.expire_grace_period = expire_grace_period;
			return data;
		}
	}
}
