package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.event.GatewayDispatch;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class GetGuildPruneCountData implements RequestData {
	protected int days;

	protected GetGuildPruneCountData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<GetGuildPruneCountData> {
		protected int days = 1;

		public Builder withDays(int days) {
			if (days < 1) {
				throw new IllegalArgumentException("days is too low (min 1)");
			}
			if (days > 7) {
				throw new IllegalArgumentException("days is too high (max 7");
			}
			this.days = days;
			return this;
		}

		@Override
		public GetGuildPruneCountData build() throws IllegalArgumentException {
			withDays(days);
			GetGuildPruneCountData data = new GetGuildPruneCountData();
			data.days = days;
			return data;
		}
	}
}
