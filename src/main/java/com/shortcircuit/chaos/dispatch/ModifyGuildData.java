package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.Utils;
import org.apache.http.HttpEntity;

import java.awt.image.RenderedImage;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class ModifyGuildData implements RequestData {
	private String name;
	private String region;
	private Integer verification_level;
	private String afk_channel_id;
	private Integer afk_timeout;
	private String icon;
	private String owner_id;
	private String splash;

	protected ModifyGuildData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return null;
	}

	public static class Builder extends RequestData.Builder<ModifyGuildData> {
		protected String name;
		protected String region;
		protected Integer verification_level;
		protected String afk_channel_id;
		protected Integer afk_timeout;
		protected RenderedImage icon;
		protected String owner_id;
		protected RenderedImage splash;

		public Builder withName(String name) {
			if (name != null) {
				if (name.length() < 2) {
					throw new IllegalArgumentException("name is too short (min 2 characters)");
				}
				if (name.length() > 100) {
					throw new IllegalArgumentException("name is too long (max 100 characters)");
				}
			}
			this.name = name;
			return this;
		}

		public Builder withoutName() {
			this.name = null;
			return this;
		}

		public Builder withRegion(String region) {
			this.region = region;
			return this;
		}

		public Builder withoutRegion() {
			this.region = null;
			return this;
		}

		public Builder withVerificationLevel(int verification_level) {
			this.verification_level = verification_level;
			return this;
		}

		public Builder withoutVerificationLevel() {
			this.verification_level = null;
			return this;
		}

		public Builder withAfkChannelId(String afk_channel_id) {
			this.afk_channel_id = afk_channel_id;
			return this;
		}

		public Builder withoutAfkChannelId() {
			this.afk_channel_id = null;
			return this;
		}

		public Builder withAfkTimeout(int afk_timeout) {
			this.afk_timeout = afk_timeout;
			return this;
		}

		public Builder withoutAfkTimeout() {
			this.afk_timeout = null;
			return this;
		}

		public Builder withIcon(RenderedImage icon) {
			if (icon != null && (icon.getWidth() != 128 || icon.getHeight() != 128)) {
				throw new IllegalArgumentException("icon mut be 128x128 pixels");
			}
			this.icon = icon;
			return this;
		}

		public Builder withoutIcon() {
			this.icon = null;
			return this;
		}

		public Builder withOwnerId(String owner_id) {
			this.owner_id = owner_id;
			return this;
		}

		public Builder withoutOwnerId() {
			this.owner_id = null;
			return this;
		}

		public Builder withSplash(RenderedImage splash) {
			if (splash != null && (splash.getWidth() != 128 || splash.getHeight() != 128)) {
				throw new IllegalArgumentException("splash mut be 128x128 pixels");
			}
			this.splash = splash;
			return this;
		}

		public Builder withoutSplash() {
			this.splash = null;
			return this;
		}

		@Override
		public ModifyGuildData build() throws IllegalArgumentException {
			withName(name);
			withIcon(icon);
			withSplash(splash);
			ModifyGuildData data = new ModifyGuildData();
			data.name = name;
			data.region = region;
			data.verification_level = verification_level;
			data.afk_channel_id = afk_channel_id;
			data.afk_timeout = afk_timeout;
			if (icon != null) {
				try {
					data.icon = Utils.imageToDataUrl(icon, "jpeg");
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
			data.owner_id = owner_id;
			if (splash != null) {
				try {
					data.splash = Utils.imageToDataUrl(splash, "jpeg");
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
			return data;
		}
	}
}
