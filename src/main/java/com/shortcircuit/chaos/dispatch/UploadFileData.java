package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.obj.Snowflake;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * @author ShortCircuit908
 *         Created on 5/26/2016.
 */
public class UploadFileData extends CreateMessageData {
	private File file;

	@Override
	public HttpEntity toEntity() throws IOException {
		MultipartEntityBuilder builder = MultipartEntityBuilder.create()
				.addBinaryBody("file", file, ContentType.create(Files.probeContentType(file.toPath())), file.getName());
		if (content != null) {
			builder.addTextBody("content", content);
		}
		if (nonce != null) {
			builder.addTextBody("nonce", nonce);
		}
		if (tts != null) {
			builder.addTextBody("tts", tts + "");
		}
		return builder.build();
	}

	public static class Builder extends CreateMessageData.Builder {
		protected File file;

		public Builder withFile(File file) {
			if (file == null) {
				throw new IllegalArgumentException("file cannot be null");
			}
			this.file = file;
			return this;
		}

		@Override
		public Builder appendContent(String content, Style... styles) {
			return (Builder) super.appendContent(content, styles);
		}

		@Override
		public Builder withContent(String content, Style... styles) {
			return (Builder) super.withContent(content, styles);
		}

		@Override
		public Builder appendCode(String language, String content, Style... styles) {
			return (Builder) super.appendCode(language, content, styles);
		}

		@Override
		public Builder withCode(String language, String content, Style... styles) {
			return (Builder) super.withCode(language, content, styles);
		}

		@Override
		public Builder appendMention(Mention mention, Snowflake snowflake, Style... styles) {
			return (Builder) super.appendMention(mention, snowflake, styles);
		}

		@Override
		public Builder withMention(Mention mention, Snowflake snowflake, Style... styles) {
			return (Builder) super.withMention(mention, snowflake, styles);
		}

		@Override
		public Builder withNonce(String nonce) {
			return (Builder) super.withNonce(nonce);
		}

		@Override
		public Builder withoutNonce() {
			return (Builder) super.withoutNonce();
		}

		@Override
		public Builder withTTS(boolean tts) {
			return (Builder) super.withTTS(tts);
		}

		@Override
		public Builder withoutTTS() {
			return (Builder) super.withoutTTS();
		}

		@Override
		public UploadFileData build() throws IllegalArgumentException {
			withContent(content.toString());
			withFile(file);
			UploadFileData data = new UploadFileData();
			data.tts = tts;
			data.nonce = nonce;
			data.content = content.toString();
			data.file = file;
			return data;
		}
	}
}
