package com.shortcircuit.chaos.dispatch;

import com.shortcircuit.chaos.Utils;
import com.shortcircuit.chaos.event.GatewayDispatch;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.awt.image.RenderedImage;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class CreateGuildData implements RequestData {
	private String name;
	private String region;
	private String icon;

	protected CreateGuildData() {

	}

	@Override
	public HttpEntity toEntity() throws IOException {
		return new StringEntity(GatewayDispatch.GSON.toJson(this));
	}

	public static class Builder extends RequestData.Builder<CreateGuildData> {
		protected String name;
		protected String region;
		protected RenderedImage icon;

		public Builder withName(String name) {
			if (name == null) {
				throw new IllegalArgumentException("name cannot be null");
			}
			if (name.length() < 2) {
				throw new IllegalArgumentException("name is too short (min 2 characters)");
			}
			if (name.length() > 100) {
				throw new IllegalArgumentException("name is too long (max 100 characters)");
			}
			this.name = name;
			return this;
		}

		public Builder withRegion(String region) {
			this.region = region;
			return this;
		}

		public Builder withIcon(RenderedImage icon) {
			if (icon != null && (icon.getWidth() != 128 || icon.getHeight() != 128)) {
				throw new IllegalArgumentException("icon mut be 128x128 pixels");
			}
			this.icon = icon;
			return this;
		}

		public Builder withoutIcon() {
			this.icon = null;
			return this;
		}

		@Override
		public CreateGuildData build() throws IllegalArgumentException {
			withName(name);
			withIcon(icon);
			CreateGuildData data = new CreateGuildData();
			data.name = name;
			data.region = region;
			if (icon != null) {
				try {
					data.icon = Utils.imageToDataUrl(icon, "jpeg");
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
			return data;
		}

	}
}
