package com.shortcircuit.chaos.obj;

import com.shortcircuit.chaos.event.GatewayDispatch;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class Game extends GatewayDispatch.EventObject {
	private String name;

	public Game(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
