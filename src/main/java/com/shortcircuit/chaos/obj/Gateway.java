package com.shortcircuit.chaos.obj;

import com.shortcircuit.chaos.event.GatewayDispatch;

/**
 * @author ShortCircuit908
 *         Created on 5/26/2016.
 */
public class Gateway extends GatewayDispatch.EventObject {
	private String url;

	public Gateway(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}
}
