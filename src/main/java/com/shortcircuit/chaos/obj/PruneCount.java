package com.shortcircuit.chaos.obj;

import com.shortcircuit.chaos.event.GatewayDispatch;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class PruneCount extends GatewayDispatch.EventObject {
	private int pruned;

	public PruneCount(int pruned) {
		this.pruned = pruned;
	}

	public int getDays() {
		return pruned;
	}
}
