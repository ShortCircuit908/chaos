package com.shortcircuit.chaos.obj.permissions;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public interface Permissions {
	int CREATE_INSTANT_INVITE = 0x00000001;
	int KICK_MEMBERS = 0x00000002;
	int BAN_MEMBERS = 0x00000004;
	int ADMINISTRATOR = 0x00000008;
	int MANAGE_CHANNELS = 0x00000010;
	int MANAGE_GUILD = 0x00000020;
	int READ_MESSAGES = 0x00000400;
	int SEND_MESSAGES = 0x00000800;
	int SEND_TTS_MESSAGES = 0x00001000;
	int MANAGE_MESSAGES = 0x00002000;
	int EMBED_LINKS = 0x00004000;
	int ATTACH_FILES = 0x00008000;
	int READ_MESSAGE_HISTORY = 0x00010000;
	int MENTION_EVERYONE = 0x00020000;
	int CONNECT = 0x00100000;
	int SPEAK = 0x00200000;
	int MUTE_MEMBERS = 0x00400000;
	int DEAFEN_MEMBERS = 0x00800000;
	int MOVE_MEMBERS = 0x01000000;
	int USE_VAD = 0x02000000;
	int CHANGE_NICKNAME = 0x04000000;
	int MANAGE_NICKNAMES = 0x08000000;
	int MANAGE_ROLES = 0x10000000;

	static boolean hasPermission(int permissions_value, int permission) {
		return (permissions_value & permission) != 0;
	}
}
