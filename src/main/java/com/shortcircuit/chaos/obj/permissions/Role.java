package com.shortcircuit.chaos.obj.permissions;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class Role extends GatewayDispatch.EventObject {
	private Snowflake id;
	private String name;
	private int color;
	private boolean hoist;
	private int position;
	private int permissions;
	private boolean managed;
	private boolean mentionable;

	protected Role() {

	}

	public Snowflake getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getColor() {
		return color;
	}

	public boolean isHoisted() {
		return hoist;
	}

	public int getPosition() {
		return position;
	}

	public int getPermissions() {
		return permissions;
	}

	public boolean hasPermission(int permission) {
		return Permissions.hasPermission(permissions, permission);
	}

	public boolean isManaged() {
		return managed;
	}

	public boolean isMentionable() {
		return mentionable;
	}
}
