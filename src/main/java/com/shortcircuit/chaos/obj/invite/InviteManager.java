package com.shortcircuit.chaos.obj.invite;

import com.shortcircuit.chaos.DiscordClient;
import com.shortcircuit.chaos.dispatch.Endpoint;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 8/23/2016.
 */
public class InviteManager {
	private final DiscordClient client;

	public InviteManager(DiscordClient client) {
		this.client = client;
	}

	public Invite getInvite(String invite_code) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_INVITE, invite_code);
	}

	public Invite deleteInvite(String invite_code) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.DELETE_INVITE, invite_code);
	}

	public Invite acceptInvite(String invite_code) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.ACCEPT_INVITE, invite_code);
	}
}
