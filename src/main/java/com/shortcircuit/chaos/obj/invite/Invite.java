package com.shortcircuit.chaos.obj.invite;

import com.shortcircuit.chaos.event.GatewayDispatch;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class Invite extends GatewayDispatch.EventObject {
	private String code;
	private InviteGuild guild;
	private InviteChannel channel;
	private String xkcdpass;

	protected Invite() {

	}

	public String getCode() {
		return code;
	}

	public InviteGuild getGuild() {
		return guild;
	}

	public InviteChannel getChannel() {
		return channel;
	}

	public String getXkcdpass() {
		return xkcdpass;
	}

	public String getInviteUrl() {
		return "https://discord.gg/" + (xkcdpass != null ? xkcdpass : code);
	}
}
