package com.shortcircuit.chaos.obj.invite;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class InviteGuild extends GatewayDispatch.EventObject {
	private Snowflake id;
	private String name;
	private String splash_hash;

	protected InviteGuild() {

	}

	public Snowflake getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSplashHash() {
		return splash_hash;
	}
}
