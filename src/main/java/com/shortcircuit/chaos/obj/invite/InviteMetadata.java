package com.shortcircuit.chaos.obj.invite;

import com.shortcircuit.chaos.obj.user.User;

import java.util.Date;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class InviteMetadata extends Invite {
	private User inviter;
	private int uses;
	private int max_uses;
	private int max_age;
	private boolean temporary;
	private Date created_at;
	private boolean revoked;

	protected InviteMetadata() {

	}

	public User getInviter() {
		return inviter;
	}

	public int getUses() {
		return uses;
	}

	public int getMaxUses() {
		return max_uses;
	}

	public int getMaxAge() {
		return max_age;
	}

	public boolean isTemporary() {
		return temporary;
	}

	public Date getCreatedAt() {
		return created_at;
	}

	public boolean isRevoked() {
		return revoked;
	}
}
