package com.shortcircuit.chaos.obj.invite;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.channel.GuildChannel;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class InviteChannel extends GatewayDispatch.EventObject {
	private Snowflake id;
	private String name;
	private GuildChannel.Type type;

	protected InviteChannel() {

	}

	public Snowflake getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public GuildChannel.Type getType() {
		return type;
	}
}
