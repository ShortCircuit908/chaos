package com.shortcircuit.chaos.obj.voice;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class VoiceRegion extends GatewayDispatch.EventObject {
	private Snowflake id;
	private String name;
	private String sample_hostname;
	private int sample_port;
	private boolean vip;
	private boolean optimal;

	protected VoiceRegion() {

	}

	public Snowflake getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSampleHostname() {
		return sample_hostname;
	}

	public int getSamplePort() {
		return sample_port;
	}

	public boolean isVip() {
		return vip;
	}

	public boolean isOptimal() {
		return optimal;
	}
}
