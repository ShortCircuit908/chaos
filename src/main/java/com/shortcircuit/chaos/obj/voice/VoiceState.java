package com.shortcircuit.chaos.obj.voice;

import com.shortcircuit.chaos.obj.Snowflake;

import java.util.UUID;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class VoiceState {
	private Snowflake guild_id;
	private Snowflake channel_id;
	private Snowflake user_id;
	private UUID session_id;
	private boolean deaf;
	private boolean mute;
	private boolean self_deaf;
	private boolean self_mute;
	private boolean suppress;

	protected VoiceState() {

	}

	public Snowflake getGuildId() {
		return guild_id;
	}

	public Snowflake getChannelId() {
		return channel_id;
	}

	public Snowflake getUserId() {
		return user_id;
	}

	public UUID getSessionId() {
		return session_id;
	}

	public boolean isDeaf() {
		return deaf;
	}

	public boolean isMute() {
		return mute;
	}

	public boolean isSelfDeaf() {
		return self_deaf;
	}

	public boolean isSelfMute() {
		return self_mute;
	}

	public boolean isSuppress() {
		return suppress;
	}
}
