package com.shortcircuit.chaos.obj.channel;

import com.google.gson.annotations.SerializedName;
import com.shortcircuit.chaos.event.Exclude;
import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GuildChannel extends GatewayDispatch.EventObject {
	@Exclude
	private VoiceChannel voice_channel;
	@Exclude
	private TextChannel text_channel;
	private Snowflake id;
	private Snowflake guild_id;
	private String name;
	private Type type;
	private int position;
	private boolean is_private;
	private Overwrite[] permission_overwrites;
	private String topic;
	private Snowflake last_message_id;
	private int bitrate;

	protected GuildChannel(Snowflake id, Snowflake guild_id, String name, Type type, int position, Overwrite[] permission_overwrites, String topic, Snowflake last_message_id, int bitrate) {
		this.id = id;
		this.guild_id = guild_id;
		this.name = name;
		this.type = type;
		this.position = position;
		this.is_private = false;
		this.permission_overwrites = permission_overwrites;
		this.topic = topic;
		this.last_message_id = last_message_id;
		this.bitrate = bitrate;
	}

	public Snowflake getId() {
		return id;
	}

	public Snowflake getGuildId() {
		return guild_id;
	}

	public String getName() {
		return name;
	}

	public Type getType() {
		return type;
	}

	public int getPosition() {
		return position;
	}

	public boolean isPrivate() {
		return is_private;
	}

	public Overwrite[] getPermissionOverwrites() {
		return permission_overwrites;
	}

	public String getTopic() {
		return topic;
	}

	public Snowflake getLastMessageId() {
		return last_message_id;
	}

	public int getBitrate() {
		return bitrate;
	}

	public boolean isTextChannel() {
		return type == Type.TEXT;
	}

	public boolean isVoiceChannel() {
		return type == Type.VOICE;
	}

	public TextChannel asTextChannel() {
		if (!isTextChannel()) {
			throw new IllegalStateException("instance is not a TextChannel");
		}
		return text_channel == null ? (text_channel = new TextChannel(id, guild_id, name, position, permission_overwrites, topic, last_message_id)) : text_channel;
	}

	public VoiceChannel asVoiceChannel() {
		if (!isVoiceChannel()) {
			throw new IllegalStateException("instance is not a VoiceChannel");
		}
		return voice_channel == null ? (voice_channel = new VoiceChannel(id, guild_id, name, position, permission_overwrites, bitrate)) : voice_channel;
	}


	public enum Type {
		@SerializedName("text")
		TEXT,
		@SerializedName("voice")
		VOICE
	}
}
