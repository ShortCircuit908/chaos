package com.shortcircuit.chaos.obj.channel;

import com.shortcircuit.chaos.event.GatewayDispatch;

/**
 * @author ShortCircuit908
 *         Created on 11/8/2016.
 */
public class EmbedFooter extends GatewayDispatch.EventObject {
	private String text;
	private String icon_url;
	private String proxy_icon_url;

	protected EmbedFooter() {

	}

	public String getText() {
		return text;
	}

	public String getIconUrl() {
		return icon_url;
	}

	public String getProxyIconUrl() {
		return proxy_icon_url;
	}
}
