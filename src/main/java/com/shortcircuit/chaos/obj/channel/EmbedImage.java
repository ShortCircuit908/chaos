package com.shortcircuit.chaos.obj.channel;

import com.shortcircuit.chaos.event.GatewayDispatch;

/**
 * @author ShortCircuit908
 *         Created on 11/8/2016.
 */
public class EmbedImage extends GatewayDispatch.EventObject {
	private String url;
	private String proxy_url;
	private int height;
	private int width;

	protected EmbedImage() {

	}

	public String getUrl() {
		return url;
	}

	public String getProxyUrl() {
		return proxy_url;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
}
