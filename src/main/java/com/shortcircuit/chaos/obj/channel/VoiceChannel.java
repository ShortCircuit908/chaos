package com.shortcircuit.chaos.obj.channel;

import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class VoiceChannel extends GuildChannel {
	public VoiceChannel(Snowflake id, Snowflake guild_id, String name, int position, Overwrite[] permission_overwrites, int bitrate) {
		super(id, guild_id, name, Type.VOICE, position, permission_overwrites, null, null, bitrate);
	}

	@Override
	public String getTopic() {
		throw new IllegalStateException("instance is not a TextChannel");
	}

	@Override
	public Snowflake getLastMessageId() {
		throw new IllegalStateException("instance is not a TextChannel");
	}
}
