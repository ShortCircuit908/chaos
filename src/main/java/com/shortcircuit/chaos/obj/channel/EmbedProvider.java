package com.shortcircuit.chaos.obj.channel;

import com.shortcircuit.chaos.event.GatewayDispatch;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class EmbedProvider extends GatewayDispatch.EventObject {
	private String name;
	private String url;

	protected EmbedProvider() {

	}

	public String getName() {
		return name;
	}

	public String getUrl() {
		return url;
	}
}
