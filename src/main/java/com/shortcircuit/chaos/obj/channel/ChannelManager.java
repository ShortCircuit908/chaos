package com.shortcircuit.chaos.obj.channel;

import com.shortcircuit.chaos.DiscordClient;
import com.shortcircuit.chaos.dispatch.*;
import com.shortcircuit.chaos.event.ChannelCreateEvent;
import com.shortcircuit.chaos.event.ChannelDeleteEvent;
import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.invite.Invite;
import com.shortcircuit.chaos.obj.invite.InviteMetadata;

import java.io.IOException;
import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 6/1/2016.
 */
public class ChannelManager {
	private final DiscordClient client;

	public ChannelManager(DiscordClient client) {
		this.client = client;
	}

	public ChannelCreateEvent.EventObject getChannel(Snowflake channel_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_CHANNEL, channel_id.getUnsignedSnowflake());
	}

	public GuildChannel modifyChannel(Snowflake channel_id, ModifyChannelData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.MODIFY_CHANNEL, data, channel_id.getUnsignedSnowflake());
	}

	public ChannelDeleteEvent.EventObject deleteChannel(Snowflake channel_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.DELETE_CHANNEL, channel_id.getUnsignedSnowflake());
	}

	public LinkedList<Message> getChannelMessages(Snowflake channel_id, GetChannelMessagesData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_CHANNEL_MESSAGES, data, channel_id.getUnsignedSnowflake());
	}

	public Message createMessage(Snowflake channel_id, CreateMessageData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.CREATE_MESSAGE, data, channel_id.getUnsignedSnowflake());
	}

	public Message uploadFile(Snowflake channel_id, UploadFileData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.UPLOAD_FILE, data, channel_id.getUnsignedSnowflake());
	}

	public Message editMessage(Snowflake channel_id, Snowflake message_id, EditMessageData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.EDIT_MESSAGE, data, channel_id.getUnsignedSnowflake(), message_id.getUnsignedSnowflake());
	}

	public Message deleteMessage(Snowflake channel_id, Snowflake message_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.DELETE_MESSAGE, channel_id.getUnsignedSnowflake(), message_id.getUnsignedSnowflake());
	}

	public void bulkDeleteMessages(Snowflake channel_id, BulkDeleteMessagesData data) throws IOException {
		client.getRequestDispatcher().dispatch(Endpoint.BULK_DELETE_MESSAGES, data, channel_id.getUnsignedSnowflake());
	}

	public void acknowledgeMessage(Snowflake channel_id, Snowflake message_id) throws IOException {
		client.getRequestDispatcher().dispatch(Endpoint.ACKNOWLEDGE_MESSAGE, channel_id.getUnsignedSnowflake(), message_id.getUnsignedSnowflake());
	}

	public void editChannelPermissions(Snowflake channel_id, Snowflake message_id, EditChannelPermissionsData data) throws IOException {
		client.getRequestDispatcher().dispatch(Endpoint.EDIT_CHANNEL_PERMISSIONS, data, channel_id.getUnsignedSnowflake(), message_id.getUnsignedSnowflake());
	}

	public LinkedList<Invite> getChannelInvites(Snowflake channel_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_CHANNEL_INVITES, channel_id.getUnsignedSnowflake());
	}

	public InviteMetadata createChannelInvite(Snowflake channel_id, CreateChannelInviteData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.CREATE_CHANNEL_INVITE, data, channel_id.getUnsignedSnowflake());
	}

	public void deleteChannelPermission(Snowflake channel_id, Snowflake permission_id) throws IOException {
		client.getRequestDispatcher().dispatch(Endpoint.DELETE_CHANNEL_PERMISSION, channel_id.getUnsignedSnowflake(), permission_id.getUnsignedSnowflake());
	}

	public void triggerTypingIndicator(Snowflake channel_id) throws IOException {
		client.getRequestDispatcher().dispatch(Endpoint.TRIGGER_TYPING_INDICATOR, channel_id.getUnsignedSnowflake());
	}

	public LinkedList<Message> getPinnedMessages(Snowflake channel_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_PINNED_MESSAGES, channel_id.getUnsignedSnowflake());
	}

	public void addPinnedChannelMessage(Snowflake channel_id, Snowflake message_id) throws IOException {
		client.getRequestDispatcher().dispatch(Endpoint.ADD_PINNED_CHANNEL_MESSAGE, channel_id.getUnsignedSnowflake(), message_id.getUnsignedSnowflake());
	}

	public void deletePinnedChannelMessage(Snowflake channel_id, Snowflake message_id) throws IOException {
		client.getRequestDispatcher().dispatch(Endpoint.DELETE_PINNED_CHANNEL_MESSAGE, channel_id.getUnsignedSnowflake(), message_id.getUnsignedSnowflake());
	}
}
