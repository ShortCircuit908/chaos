package com.shortcircuit.chaos.obj.channel;

import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class TextChannel extends GuildChannel {
	public TextChannel(Snowflake id, Snowflake guild_id, String name, int position, Overwrite[] permission_overwrites, String topic, Snowflake last_message_id) {
		super(id, guild_id, name, Type.TEXT, position, permission_overwrites, topic, last_message_id, 0);
	}

	@Override
	public int getBitrate() {
		throw new IllegalStateException("instance is not a VoiceChannel");
	}
}
