package com.shortcircuit.chaos.obj.channel;

import com.google.gson.annotations.SerializedName;
import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.permissions.Permissions;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class Overwrite extends GatewayDispatch.EventObject {
	private Snowflake id;
	private Type type;
	private int allow;
	private int deny;

	protected Overwrite() {

	}

	public Snowflake getId() {
		return id;
	}

	public Type getType() {
		return type;
	}

	public int getAllow() {
		return allow;
	}

	public boolean hasAllowPermission(int permission) {
		return Permissions.hasPermission(allow, permission);
	}

	public int getDeny() {
		return deny;
	}

	public boolean hasDenyPermission(int permission) {
		return Permissions.hasPermission(deny, permission);
	}

	public enum Type {
		@SerializedName("role")
		ROLE,
		@SerializedName("member")
		MEMBER
	}
}
