package com.shortcircuit.chaos.obj.channel;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.user.User;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class DMChannel extends GatewayDispatch.EventObject {
	private Snowflake id;
	private boolean is_private;
	private User recipient;
	private Snowflake last_message_id;

	public DMChannel(Snowflake id, User recipient, Snowflake last_message_id) {
		this.id = id;
		this.is_private = true;
		this.recipient = recipient;
		this.last_message_id = last_message_id;
	}

	public Snowflake getId() {
		return id;
	}

	public boolean isPrivate() {
		return is_private;
	}

	public User getRecipient() {
		return recipient;
	}

	public Snowflake getLastMessageId() {
		return last_message_id;
	}
}
