package com.shortcircuit.chaos.obj.channel;

import com.shortcircuit.chaos.event.GatewayDispatch;

/**
 * @author ShortCircuit908
 *         Created on 11/8/2016.
 */
public class EmbedAuthor extends GatewayDispatch.EventObject {
	private String name;
	private String url;
	private String icon_url;
	private String proxy_icon_url;

	protected EmbedAuthor() {

	}

	public String getName() {
		return name;
	}

	public String getUrl() {
		return url;
	}

	public String getIconUrl() {
		return icon_url;
	}

	public String getProxyIconUrl() {
		return proxy_icon_url;
	}
}
