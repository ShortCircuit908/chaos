package com.shortcircuit.chaos.obj.channel;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class Attachment extends GatewayDispatch.EventObject {
	private Snowflake id;
	private String filename;
	private long size;
	private String url;
	private String proxy_url;
	private int height;
	private int width;

	protected Attachment() {

	}

	public Snowflake getId() {
		return id;
	}

	public String getFileName() {
		return filename;
	}

	public long getFileSize() {
		return size;
	}

	public String getUrl() {
		return url;
	}

	public String getProxyUrl() {
		return proxy_url;
	}

	public int getImageHeight() {
		return height;
	}

	public int getImageWidth() {
		return width;
	}
}
