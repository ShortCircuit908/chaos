package com.shortcircuit.chaos.obj.channel;

import com.shortcircuit.chaos.event.GatewayDispatch;

/**
 * @author ShortCircuit908
 *         Created on 11/8/2016.
 */
public class EmbedVideo extends GatewayDispatch.EventObject {
	private String url;
	private int height;
	private int width;

	protected EmbedVideo() {

	}

	public String getUrl() {
		return url;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
}
