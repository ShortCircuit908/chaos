package com.shortcircuit.chaos.obj.channel;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.user.User;

import java.util.Date;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class Message extends GatewayDispatch.EventObject {
	private Snowflake id;
	private Snowflake channel_id;
	private User author;
	private String content;
	private Date timestamp;
	private Date edited_timestamp;
	private boolean tts;
	private boolean mention_everyone;
	private User[] mentions;
	private Snowflake[] mention_roles;
	private Attachment[] attachments;
	private Embed[] embeds;
	private String nonce;
	private boolean pinned;
	private Snowflake webhook_id;

	protected Message() {

	}

	public Snowflake getId() {
		return id;
	}

	public Snowflake getChannelId() {
		return channel_id;
	}

	public User getAuthor() {
		return author;
	}

	public String getContent() {
		return content;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public Date getEditedTimestamp() {
		return edited_timestamp;
	}

	public boolean isTextToSpeech() {
		return tts;
	}

	public boolean mentionsEveryone() {
		return mention_everyone;
	}

	public User[] getMentions() {
		return mentions;
	}

	public Snowflake[] getRoleMentions() {
		return mention_roles;
	}

	public Attachment[] getAttachments() {
		return attachments;
	}

	public Embed[] getEmbeds() {
		return embeds;
	}

	public String getNonce() {
		return nonce;
	}

	public boolean isPinned() {
		return pinned;
	}
}
