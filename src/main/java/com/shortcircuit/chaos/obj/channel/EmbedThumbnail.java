package com.shortcircuit.chaos.obj.channel;

import com.shortcircuit.chaos.event.GatewayDispatch;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class EmbedThumbnail extends GatewayDispatch.EventObject {
	private String url;
	private String proxy_url;
	private int height;
	private int width;

	protected EmbedThumbnail() {

	}

	public String getUrl() {
		return url;
	}

	public String getProxyUrl() {
		return proxy_url;
	}

	public int getImageWidth() {
		return width;
	}

	public int getImageHeight() {
		return height;
	}
}
