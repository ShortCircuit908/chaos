package com.shortcircuit.chaos.obj.channel;

import com.shortcircuit.chaos.event.GatewayDispatch;

/**
 * @author ShortCircuit908
 *         Created on 11/8/2016.
 */
public class EmbedField extends GatewayDispatch.EventObject {
	private String name;
	private String value;
	private boolean inline;

	protected EmbedField() {

	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	public boolean isInline() {
		return inline;
	}
}
