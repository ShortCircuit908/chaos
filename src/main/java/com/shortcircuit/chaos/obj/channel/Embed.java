package com.shortcircuit.chaos.obj.channel;

import com.shortcircuit.chaos.event.GatewayDispatch;

import java.util.Date;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class Embed extends GatewayDispatch.EventObject {
	private String title;
	private String type;
	private String description;
	private String url;
	private Date timestamp;
	private int color;
	private EmbedFooter footer;
	private EmbedImage image;
	private EmbedThumbnail thumbnail;
	private EmbedVideo video;
	private EmbedProvider provider;
	private EmbedAuthor author;
	private EmbedField[] fields;

	protected Embed() {

	}

	public String getTitle() {
		return title;
	}

	public String getType() {
		return type;
	}

	public String getDescription() {
		return description;
	}

	public String getUrl() {
		return url;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public int getColor() {
		return color;
	}

	public EmbedFooter getFooter() {
		return footer;
	}

	public EmbedImage getImage() {
		return image;
	}

	public EmbedThumbnail getThumbnail() {
		return thumbnail;
	}

	public EmbedVideo getVideo() {
		return video;
	}

	public EmbedProvider getProvider() {
		return provider;
	}

	public EmbedAuthor getAuthor() {
		return author;
	}

	public EmbedField[] getFields() {
		return fields;
	}
}
