package com.shortcircuit.chaos.obj.guild;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.permissions.Role;
import com.shortcircuit.chaos.obj.voice.VoiceState;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class Guild extends GatewayDispatch.EventObject {
	private Snowflake id;
	private String name;
	private String icon;
	private String splash;
	private Snowflake owner_id;
	private String region;
	private Snowflake afk_channel_id;
	private int afk_timeout;
	private boolean embed_enabled;
	private Snowflake embed_channel_id;
	private int verification_level;
	private VoiceState[] voice_states;
	private Role[] roles;
	private Emoji[] emojis;
	private String[] features;

	protected Guild() {

	}

	public Snowflake getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getIcon() {
		return icon;
	}

	public String getSplash() {
		return splash;
	}

	public Snowflake getOwnerId() {
		return owner_id;
	}

	public String getRegion() {
		return region;
	}

	public Snowflake getAfkChannelId() {
		return afk_channel_id;
	}

	public int getAfkTimeout() {
		return afk_timeout;
	}

	public boolean isEmbedEnabled() {
		return embed_enabled;
	}

	public Snowflake getEmbedChannelId() {
		return embed_channel_id;
	}

	public int getVerificationLevel() {
		return verification_level;
	}

	public VoiceState[] getVoiceStates() {
		return voice_states;
	}

	public Role[] getRoles() {
		return roles;
	}

	public Emoji[] getEmojis() {
		return emojis;
	}

	public String[] getFeatures() {
		return features;
	}
}
