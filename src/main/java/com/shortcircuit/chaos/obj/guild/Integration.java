package com.shortcircuit.chaos.obj.guild;

import com.google.gson.annotations.SerializedName;
import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.user.User;

import java.util.Date;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class Integration extends GatewayDispatch.EventObject {
	private Snowflake id;
	private String name;
	private Type type;
	private boolean enabled;
	private boolean syncing;
	private Snowflake role_id;
	private int expire_behavior;
	private int expire_grace_period;
	private User user;
	private IntegrationAccount account;
	private Date synced_at;

	protected Integration() {

	}

	public Snowflake getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Type getType() {
		return type;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public boolean isSyncing() {
		return syncing;
	}

	public Snowflake getRoleId() {
		return role_id;
	}

	public int getExpireBehavior() {
		return expire_behavior;
	}

	public int getExpireGracePeriod() {
		return expire_grace_period;
	}

	public User getUser() {
		return user;
	}

	public IntegrationAccount getAccount() {
		return account;
	}

	public Date getSyncedAt() {
		return synced_at;
	}

	public enum Type {
		@SerializedName("twitch")
		TWITCH,
		@SerializedName("youtube")
		YOUTUBE
	}
}
