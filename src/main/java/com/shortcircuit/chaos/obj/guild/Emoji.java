package com.shortcircuit.chaos.obj.guild;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.permissions.Role;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class Emoji extends GatewayDispatch.EventObject {
	private Snowflake id;
	private String name;
	private Role[] roles;
	private boolean require_colons;
	private boolean managed;

	protected Emoji() {

	}

	public Snowflake getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Role[] getRoles() {
		return roles;
	}

	public boolean requiresColons() {
		return require_colons;
	}

	public boolean isManaged() {
		return managed;
	}
}
