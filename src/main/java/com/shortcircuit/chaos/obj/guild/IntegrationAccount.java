package com.shortcircuit.chaos.obj.guild;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class IntegrationAccount extends GatewayDispatch.EventObject {
	private Snowflake id;
	private String name;

	protected IntegrationAccount() {

	}

	public Snowflake getId() {
		return id;
	}

	public String getName() {
		return name;
	}
}
