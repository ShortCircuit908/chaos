package com.shortcircuit.chaos.obj.guild;

import com.shortcircuit.chaos.DiscordClient;
import com.shortcircuit.chaos.dispatch.*;
import com.shortcircuit.chaos.obj.PruneCount;
import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.channel.GuildChannel;
import com.shortcircuit.chaos.obj.invite.InviteMetadata;
import com.shortcircuit.chaos.obj.permissions.Role;
import com.shortcircuit.chaos.obj.user.User;
import com.shortcircuit.chaos.obj.voice.VoiceRegion;

import java.io.IOException;
import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 6/1/2016.
 */
public class GuildManager {
	private final DiscordClient client;

	public GuildManager(DiscordClient client) {
		this.client = client;
	}

	public Guild createGuild(CreateGuildData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.CREATE_GUILD, data);
	}

	public Guild getGuild(Snowflake guild_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_GUILD, guild_id.getUnsignedSnowflake());
	}

	public Guild modifyGuild(Snowflake guild_id, ModifyGuildData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.MODIFY_GUILD, data, guild_id.getUnsignedSnowflake());
	}

	public Guild deleteGuild(Snowflake guild_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.DELETE_GUILD, guild_id.getUnsignedSnowflake());
	}

	public LinkedList<GuildChannel> getGuildChannels(Snowflake guild_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_GUILD_CHANNELS, guild_id.getUnsignedSnowflake());
	}

	public GuildChannel createGuildChannel(Snowflake guild_id, CreateGuildData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.CREATE_GUILD_CHANNEL, data, guild_id.getUnsignedSnowflake());
	}

	public GuildChannel modifyGuildChannel(Snowflake guild_id, ModifyGuildChannelData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.MODIFY_GUILD_CHANNEL, data, guild_id.getUnsignedSnowflake());
	}

	public GuildMember getGuildMember(Snowflake guild_id, Snowflake member_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_GUILD_MEMBER, guild_id.getUnsignedSnowflake(), member_id.getUnsignedSnowflake());
	}

	public LinkedList<GuildMember> listGuildMembers(Snowflake guild_id, ListGuildMembersData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.LIST_GUILD_MEMBERS, data, guild_id.getUnsignedSnowflake());
	}

	public GuildMember modifyGuildMember(Snowflake guild_id, Snowflake member_id, ModifyGuildMemberData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.MODIFY_GUILD_MEMBER, data, guild_id.getUnsignedSnowflake(), member_id.getUnsignedSnowflake());
	}

	public void removeGuildMember(Snowflake guild_id, Snowflake member_id) throws IOException {
		client.getRequestDispatcher().dispatch(Endpoint.REMOVE_GUILD_MEMBER, guild_id.getUnsignedSnowflake(), member_id.getUnsignedSnowflake());
	}

	public LinkedList<User> getGuildBans(Snowflake guild_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_GUILD_BANS, guild_id.getUnsignedSnowflake());
	}

	public void createGuildBan(Snowflake guild_id, Snowflake member_id, CreateGuildBanData data) throws IOException {
		client.getRequestDispatcher().dispatch(Endpoint.CREATE_GUILD_BAN, data, guild_id.getUnsignedSnowflake(), member_id.getUnsignedSnowflake());
	}

	public void removeGuildBan(Snowflake guild_id, Snowflake member_id) throws IOException {
		client.getRequestDispatcher().dispatch(Endpoint.REMOVE_GUILD_BAN, guild_id.getUnsignedSnowflake(), member_id.getUnsignedSnowflake());
	}

	public LinkedList<Role> getGuildRoles(Snowflake guild_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_GUILD_ROLES, guild_id.getUnsignedSnowflake());
	}

	public Role createGuildRole(Snowflake guild_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.CREATE_GUILD_ROLE, guild_id.getUnsignedSnowflake());
	}

	public LinkedList<Role> batchModifyGuildRole(Snowflake guild_id, BatchModifyGuildRoleData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.BATCH_MODIFY_GUILD_ROLE, data, guild_id.getUnsignedSnowflake());
	}

	public Role modifyGuildRole(Snowflake guild_id, Snowflake role_id, ModifyGuildRoleData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.MODIFY_GUILD_ROLE, data, guild_id.getUnsignedSnowflake(), role_id.getUnsignedSnowflake());
	}

	public Role deleteGuildRole(Snowflake guild_id, Snowflake role_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.DELETE_GUILD_ROLE, guild_id.getUnsignedSnowflake(), role_id.getUnsignedSnowflake());
	}

	public PruneCount getGuildPruneCount(Snowflake guild_id, GetGuildPruneCountData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_GUILD_PRUNE_COUNT, data, guild_id.getUnsignedSnowflake());
	}

	public PruneCount beginGuildPrune(Snowflake guild_id, BeginGuildPruneData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.BEGIN_GUILD_PRUNE, data, guild_id.getUnsignedSnowflake());
	}

	public LinkedList<VoiceRegion> getGuildVoiceRegions(Snowflake guild_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_GUILD_VOICE_REGIONS, guild_id.getUnsignedSnowflake());
	}

	public LinkedList<InviteMetadata> getGuildInvites(Snowflake guild_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_GUILD_INVITES, guild_id.getUnsignedSnowflake());
	}

	public LinkedList<Integration> getGuildIntegrations(Snowflake guild_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_GUILD_INTEGRATIONS, guild_id.getUnsignedSnowflake());
	}

	public void createGuildIntegration(Snowflake guild_id, CreateGuildIntegrationData data) throws IOException {
		client.getRequestDispatcher().dispatch(Endpoint.CREATE_GUILD_INTEGRATION, data, guild_id.getUnsignedSnowflake());
	}

	public void modifyGuildIntegration(Snowflake guild_id, Snowflake integration_id, ModifyGuildIntegrationData data) throws IOException {
		client.getRequestDispatcher().dispatch(Endpoint.MODIFY_GUILD_INTEGRATION, data, guild_id.getUnsignedSnowflake(), integration_id.getUnsignedSnowflake());
	}

	public void deleteGuildIntegration(Snowflake guild_id, Snowflake integration_id) throws IOException {
		client.getRequestDispatcher().dispatch(Endpoint.DELETE_GUILD_INTEGRATION, guild_id.getUnsignedSnowflake(), integration_id.getUnsignedSnowflake());
	}

	public void syncGuildIntegration(Snowflake guild_id, Snowflake integration_id) throws IOException {
		client.getRequestDispatcher().dispatch(Endpoint.SYNC_GUILD_INTEGRATION, guild_id.getUnsignedSnowflake(), integration_id.getUnsignedSnowflake());
	}

	public GuildEmbed getGuildEmbed(Snowflake guild_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_GUILD_EMBED, guild_id.getUnsignedSnowflake());
	}

	public GuildEmbed modifyGuildEmbed(Snowflake guild_id, ModifyGuildEmbedData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.MODIFY_GUILD_EMBED, data, guild_id.getUnsignedSnowflake());
	}
}
