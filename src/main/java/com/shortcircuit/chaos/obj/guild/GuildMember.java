package com.shortcircuit.chaos.obj.guild;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.user.User;

import java.util.Date;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GuildMember extends GatewayDispatch.EventObject {
	private User user;
	private String nick;
	private Snowflake[] roles;
	private Date joined_at;
	private boolean deaf;
	private boolean mute;

	protected GuildMember() {

	}

	public User getUser() {
		return user;
	}

	public String getNickname() {
		return nick;
	}

	public Snowflake[] getRoles() {
		return roles;
	}

	public Date getJoinedAt() {
		return joined_at;
	}

	public boolean isDeaf() {
		return deaf;
	}

	public boolean isMute() {
		return mute;
	}
}
