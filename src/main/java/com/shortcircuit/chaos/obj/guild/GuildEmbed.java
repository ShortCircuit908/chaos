package com.shortcircuit.chaos.obj.guild;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class GuildEmbed extends GatewayDispatch.EventObject {
	private boolean enabled;
	private Snowflake channel_id;

	protected GuildEmbed() {

	}

	public boolean isEnabled() {
		return enabled;
	}

	public Snowflake getChannelId() {
		return channel_id;
	}
}
