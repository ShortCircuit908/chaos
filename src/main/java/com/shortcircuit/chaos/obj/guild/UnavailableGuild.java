package com.shortcircuit.chaos.obj.guild;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class UnavailableGuild extends GatewayDispatch.EventObject {
	private Snowflake id;
	private boolean unavailable;

	protected UnavailableGuild() {

	}

	public Snowflake getId() {
		return id;
	}

	public boolean isUnavailable() {
		return unavailable;
	}
}
