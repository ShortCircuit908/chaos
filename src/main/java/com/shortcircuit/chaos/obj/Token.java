package com.shortcircuit.chaos.obj;

import java.util.Arrays;

/**
 * @author ShortCircuit908
 *         Created on 5/27/2016.
 */
public class Token {
	private char[] token;

	@Deprecated
	public Token(String token) {
		this(token.toCharArray());
	}

	public Token(char[] token) {
		this.token = token;
	}

	public char[] getToken() {
		return token;
	}

	public void nullOut() {
		token = null;
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		nullOut();
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof Token && Arrays.equals(token, ((Token) o).token);
	}
}
