package com.shortcircuit.chaos.obj;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Locale;

/**
 * @author ShortCircuit908
 *         Created on 9/13/2016.
 */
public class ClientSettings {
	private Theme theme;
	private boolean show_current_game;
	private Snowflake[] restricted_guilds;
	private boolean render_embeds;
	private boolean message_display_compact;
	private Locale locale;
	private boolean inline_embed_media;
	private boolean inline_attachment_media;
	private Snowflake[] guild_positions;
	private HashMap<String, Boolean> friend_source_flags;
	private boolean enable_tts_command;
	private boolean developer_mode;
	private boolean detect_platform_accounts;
	private boolean convert_emoticons;
	private boolean allow_email_friend_request;

	public enum Theme{
		@SerializedName("dark")
		DARK,
		@SerializedName("light")
		LIGHT
	}
}
