package com.shortcircuit.chaos.obj.user;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class User extends GatewayDispatch.EventObject {
	private Snowflake id;
	private String username;
	private String discriminator;
	private String avatar;
	private boolean verified;
	private String email;
	private boolean bot;

	protected User() {

	}

	public Snowflake getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public String getDiscriminator() {
		return discriminator;
	}

	public String getAvatar() {
		return avatar;
	}

	public boolean isVerified() {
		return verified;
	}

	public String getEmail() {
		return email;
	}

	public boolean isBot() {
		return bot;
	}
}
