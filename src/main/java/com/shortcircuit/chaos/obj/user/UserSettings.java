package com.shortcircuit.chaos.obj.user;

import com.shortcircuit.chaos.event.GatewayDispatch;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class UserSettings extends GatewayDispatch.EventObject {
	private String username;
	private String avatar;

	protected UserSettings() {

	}

	public String getUsername() {
		return username;
	}

	public String getAvatar() {
		return avatar;
	}
}
