package com.shortcircuit.chaos.obj.user;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.guild.Integration;

import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 7/5/2016.
 */
public class Connection extends GatewayDispatch.EventObject {
	private Snowflake id;
	private String name;
	private Integration.Type type;
	private boolean revoked;
	private LinkedList<Integration> integrations;

	protected Connection(){

	}

	public Snowflake getId(){
		return id;
	}

	public String getName(){
		return name;
	}

	public Integration.Type getType(){
		return type;
	}

	public boolean isRevoked(){
		return revoked;
	}

	public LinkedList<Integration> getIntegrations(){
		return integrations;
	}
}
