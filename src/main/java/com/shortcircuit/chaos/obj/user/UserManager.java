package com.shortcircuit.chaos.obj.user;

import com.shortcircuit.chaos.DiscordClient;
import com.shortcircuit.chaos.dispatch.CreateDMData;
import com.shortcircuit.chaos.dispatch.Endpoint;
import com.shortcircuit.chaos.dispatch.ModifyCurrentUserData;
import com.shortcircuit.chaos.dispatch.QueryUsersData;
import com.shortcircuit.chaos.obj.Snowflake;
import com.shortcircuit.chaos.obj.channel.DMChannel;

import java.io.IOException;
import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 6/1/2016.
 */
public class UserManager {
	private final DiscordClient client;

	public UserManager(DiscordClient client) {
		this.client = client;
	}

	public LinkedList<User> queryUsers(QueryUsersData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.QUERY_USERS, data);
	}

	public User getCurrentUser() throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_CURRENT_USER);
	}

	public User getUser(Snowflake user_id) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_USER, user_id.getUnsignedSnowflake());
	}

	public User modifyCurrentUser(ModifyCurrentUserData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.MODIFY_CURRENT_USER, data);
	}

	public LinkedList<UserGuild> getCurrentUserGuilds() throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_CURRENT_USER_GUILDS);
	}

	public void leaveGuild(Snowflake guild_id) throws IOException {
		client.getRequestDispatcher().dispatch(Endpoint.LEAVE_GUILD, guild_id.getUnsignedSnowflake());
	}

	public LinkedList<DMChannel> getUserDMs() throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_USER_DMS);
	}

	public DMChannel createDM(CreateDMData data) throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.CREATE_DM, data);
	}

	public LinkedList<Connection> getUsersConnections() throws IOException {
		return client.getRequestDispatcher().dispatch(Endpoint.GET_USERS_CONNECTIONS);
	}
}
