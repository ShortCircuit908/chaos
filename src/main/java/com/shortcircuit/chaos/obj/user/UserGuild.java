package com.shortcircuit.chaos.obj.user;

import com.shortcircuit.chaos.event.GatewayDispatch;
import com.shortcircuit.chaos.obj.Snowflake;

/**
 * @author ShortCircuit908
 *         Created on 5/25/2016.
 */
public class UserGuild extends GatewayDispatch.EventObject {
	private Snowflake id;
	private String name;
	private String icon;
	private boolean owner;
	private int permissions;

	protected UserGuild() {

	}

	public Snowflake getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getIcon() {
		return icon;
	}

	public boolean isOwner() {
		return owner;
	}

	public int getPermissions() {
		return permissions;
	}

	public boolean hasPermission(int permission) {
		return (permissions & permission) != 0;
	}
}
