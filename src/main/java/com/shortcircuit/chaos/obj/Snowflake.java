package com.shortcircuit.chaos.obj;

/**
 * @author ShortCircuit908
 *         Created on 6/7/2016.
 */
public class Snowflake implements Comparable<Snowflake> {
	private final long snowflake;

	public Snowflake(String snowflake) throws NumberFormatException {
		this(Long.parseUnsignedLong(snowflake));
	}

	public Snowflake(long snowflake) {
		this.snowflake = snowflake;
	}

	public long getSignedSnowflake() {
		return snowflake;
	}

	public String getUnsignedSnowflake() {
		return Long.toUnsignedString(snowflake);
	}

	@Override
	public int compareTo(Snowflake o) {
		return Long.compareUnsigned(snowflake, o.snowflake);
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof Snowflake && ((Snowflake) o).snowflake == snowflake;
	}

	@Override
	public String toString() {
		return getUnsignedSnowflake();
	}
}
