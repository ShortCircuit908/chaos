package com.shortcircuit.chaos;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * @author ShortCircuit908
 *         Created on 6/7/2016.
 */
public class SimpleLogFormat extends Formatter {
	private static final SimpleDateFormat date_format = new SimpleDateFormat("MM/dd/YYYY HH:mm:ss.SSSS");
	private static final String format = "[%1$s] [%2$s] [%3$s/%4$s]: %5$s\n";

	@Override
	public String format(LogRecord record) {
		Date date = new Date(record.getMillis());
		ThreadGroup parent_group = Thread.currentThread().getThreadGroup();
		while (parent_group.getParent() != null) {
			parent_group = parent_group.getParent();
		}
		Thread[] threads = new Thread[(int) Math.ceil(parent_group.activeCount() * 1.5)];
		int thread_num = Thread.enumerate(threads);
		String thread_name = "Thread-" + record.getThreadID();
		for (int i = 0; i < thread_num; i++) {
			if (threads[i].getId() == record.getThreadID()) {
				thread_name = threads[i].getName();
				break;
			}
		}
		return String.format(format, date_format.format(date), record.getLoggerName(), thread_name, record.getLevel(), record.getMessage());
	}
}
