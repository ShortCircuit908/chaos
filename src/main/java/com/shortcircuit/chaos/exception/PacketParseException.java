package com.shortcircuit.chaos.exception;

/**
 * @author ShortCircuit908
 *         Created on 5/31/2016.
 */
public class PacketParseException extends DiscordException {
	private final String source;

	public PacketParseException(String source) {
		this(source, null);
	}

	public PacketParseException(String source, Throwable cause) {
		super("Could not parse packet from source: " + source, cause);
		this.source = source;
	}

	public String getSource() {
		return source;
	}
}
