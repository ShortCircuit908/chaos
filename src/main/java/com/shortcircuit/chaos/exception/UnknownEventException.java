package com.shortcircuit.chaos.exception;

/**
 * @author ShortCircuit908
 *         Created on 11/8/2016.
 */
public class UnknownEventException extends DiscordException {
	public UnknownEventException(String event_json){
		super("Received unknown event: " + event_json);
	}
}
