package com.shortcircuit.chaos.exception;

/**
 * @author ShortCircuit908
 *         Created on 5/31/2016.
 */
public class ClientShutDownException extends DiscordException {
	public ClientShutDownException() {
		super("Client has been shut down");
	}
}
