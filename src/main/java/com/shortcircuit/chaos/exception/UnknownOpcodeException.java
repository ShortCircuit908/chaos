package com.shortcircuit.chaos.exception;

/**
 * @author ShortCircuit908
 *         Created on 5/31/2016.
 */
public class UnknownOpcodeException extends DiscordException {
	private final int opcode;
	private final String source;

	public UnknownOpcodeException(int opcode, String source) {
		super("Unknown opcode " + opcode + " from source: " + source);
		this.opcode = opcode;
		this.source = source;
	}

	public int getOpcode() {
		return opcode;
	}

	public String getSource() {
		return source;
	}
}
