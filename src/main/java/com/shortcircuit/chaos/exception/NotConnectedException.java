package com.shortcircuit.chaos.exception;

/**
 * @author ShortCircuit908
 *         Created on 5/31/2016.
 */
public class NotConnectedException extends DiscordException {
	public NotConnectedException() {
		super("Client is not connected");
	}
}
