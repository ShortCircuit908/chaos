package com.shortcircuit.chaos.exception;

import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 5/31/2016.
 */
public class DiscordException extends IOException {
	private int code = -1;
	private String message;

	public DiscordException() {
		super();
	}

	public DiscordException(int code) {
		this("Code: " + code);
	}


	public DiscordException(String message) {
		super(message);
		this.message = message;
	}

	public DiscordException(Throwable cause) {
		super(cause);
	}

	public DiscordException(int code, String message) {
		super(message);
		this.message = message;
		this.code = code;
	}

	public DiscordException(String message, Throwable cause) {
		super(message, cause);
		this.message = message;
	}

	public DiscordException(int code, Throwable cause) {
		super("Code: " + code, cause);
		this.message = "Code: " + code;
	}

	public DiscordException(int code, String message, Throwable cause) {
		super(message, cause);
		this.message = message;
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
