package com.shortcircuit.chaos.exception;

/**
 * @author ShortCircuit908
 *         Created on 5/31/2016.
 */
public class ConnectedException extends DiscordException {
	public ConnectedException() {
		super("Client is already connected");
	}
}
